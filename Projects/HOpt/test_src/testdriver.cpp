/*
 * testdriver.cpp
 *
 *  Created on: May 23, 2014
 *      Author: panos
 */
//
#include <random>
#include <memory>
#include <glog/logging.h>
#include <zmq.hpp>
#include <stdio.h>
#include <time.h>
#include "IpIpoptApplication.hpp"
#include <math.h>
#include "GraphPts.pb.h"
#include <stdexcept>
#include <sys/time.h>
#include <ctime>
#include "zhelpers.hpp"
#include "nr3.h" //This file needs to be close to the bottom
#include "OptCtlUncMinToy.h"
#include "RKF45OpenCLTest.h"
#include "PCHIPGPUTest.h"
#include "VanDerPolGpu.h"
#include "VanDerPolNLP.h"
#include "TCanvas.h"
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TApplication.h"

namespace hyper {

int runPublisher() {
	// Prepare our context and publisher
	zmq::context_t context(1);
	zmq::socket_t publisher(context, ZMQ_PUB);
	publisher.bind("tcp://*:5556");

	// Initialize random number generator
	srandom((unsigned) time(NULL));

	while (true) {
		GraphPoints gpts;
		GraphPoints_xcoords* xc = new GraphPoints_xcoords();
		GraphPoints_ycoords* yc = new GraphPoints_ycoords();
		for (int i = 0; i < 100; i++) {
			xc->add_x(random());
			yc->add_y(random());
		}
		gpts.set_allocated_xcords(xc);
		gpts.set_allocated_ycords(yc);

		//Serialize the message
		std::string ptsStr;

		gpts.SerializeToString(&ptsStr);
		int strLen = ptsStr.length();
		zmq::message_t message(strLen);
		memcpy(message.data(), ptsStr.c_str(), strLen);
		s_sendmore(publisher, "M");
		s_send(publisher, ptsStr);

	}
	return 0;

}
};

/* Returns the amount of milliseconds elapsed since the UNIX epoch. Works on both
 * windows and linux. */

int GetTimeMs() {
#ifdef WIN32
	/* Windows */
	FILETIME ft;
	LARGE_INTEGER li;

	/* Get the amount of 100 nano seconds intervals elapsed since January 1, 1601 (UTC) and copy it
	 * to a LARGE_INTEGER structure. */
	GetSystemTimeAsFileTime(&ft);
	li.LowPart = ft.dwLowDateTime;
	li.HighPart = ft.dwHighDateTime;

	uint64 ret = li.QuadPart;
	ret -= 116444736000000000LL; /* Convert from file time to UNIX epoch time. */
	ret /= 10000; /* From 100 nano seconds (10^-7) to 1 millisecond (10^-3) intervals */

	return ret;
#else
	/* Linux */
	struct timeval tv;

	gettimeofday(&tv, NULL);

	uint ret = tv.tv_usec;
	/* Convert from micro seconds (10^-6) to milliseconds (10^-3) */
	ret /= 1000;

	/* Adds the seconds (10^0) after converting them to milliseconds (10^-3) */
	ret += (tv.tv_sec * 1000);

	return ret;
#endif
}

//These series of tests were written without using a framework for the following reasons:
//	1.  The testing frameworks out thersweeping zene today do not work well with real time systems
//	2.  Frameworks make extensive use of macros which has caused problems during runtime (in particular google test will not pick up any of the real time tests)
//  3.  It is more flexible to write a test suite in normal C++ code versus using more restrictive macros for real time messaging-exception messaging can be more explicit

void printPassMessage(const char* testName, int time) {
	std::cout << "-----------------------------------------------------------"  << std::endl;
	std::cout << "Test " << testName << " PASSED" << std::endl;
	std::cout << "Time elapsed for test serverTest=" << time << " ms\n" << std::endl;
	std::cout << "-----------------------------------------------------------";

}

void printFailMessage(const char* testName, const char* message) {
	std::cout << "-----------------------------------------------------------" << std::endl;
	std::cout << "Test " << testName << " FAILED" << std::endl;
	std::cout << "Reported error: " << message << std::endl;
	std::cout << "-----------------------------------------------------------" << std::endl;

}

void serverTest() {

	int now = GetTimeMs();

	uint count = hyper::runPublisher();
	if (count != 0) {
		throw std::runtime_error("Test serverTest failed");
	}
	int later = GetTimeMs();
	printPassMessage("serverTest", later - now);
}

void solveUncToyProbIpopt() {
	try {
	int now = GetTimeMs();
	SmartPtr<TNLP> nlp = new hyper::OptCtlUncMinToy(128);
	// Create a new instance of IpoptApplication
	//  (use a SmartPtr, not raw)
	// We are using the factory, since this allows us to compile this
	// example with an Ipopt Windows DLL
	SmartPtr <IpoptApplication> app = IpoptApplicationFactory();
	app->RethrowNonIpoptException(true);

	// Change some options
	// Note: The following choices are only examples, they might not be
	//       suitable for your optimization problem.

	app->Options()->SetStringValue("mu_strategy", "adaptive");
	app->Options()->SetStringValue("output_file", "ipopt.out");
	app->Options()->SetStringValue("hessian_approximation","limited-memory");
	app->Options()->SetStringValue("check_derivatives_for_naninf", "yes");
	app->Options()->SetIntegerValue("max_iter", 100000);
	app->Options()->SetNumericValue("tol", 1e-5);
	//app->Options()->SetStringValue("derivative_test", "first-order");
	//app->Options()->SetNumericValue("derivative_test_perturbation", 1e-8);
	// The following overwrites the default name (ipopt.opt) of the
	// options file
	// app->Options()->SetStringValue("option_file_name", "hs071.opt");

	// Initialize the IpoptApplication and process the options
	ApplicationReturnStatus status;
	status = app->Initialize();
	if (status != Solve_Succeeded) {
		std::cout << std::endl << std::endl
				<< "*** Error during initialization!" << std::endl;
	}


	// Ask Ipopt to solve the problem
	status = app->OptimizeTNLP(nlp);
	if (status == Solve_Succeeded || status == Solved_To_Acceptable_Level) {
		std::cout << std::endl << std::endl << "*** The problem solved!"
				<< std::endl;

	} else {
		std::cout << std::endl << std::endl << "*** The problem FAILED!"
				<< std::endl;

	}
	int later = GetTimeMs();
	printPassMessage("rfk45Test", later-now);

	}
	catch ( std::runtime_error& re) {
		printFailMessage("rfk45Test", re.what());

	}
}




//Need to predeclare the static variables see
//http://stackoverflow.com/questions/18433752/c-access-private-static-member-from-public-static-method
int hyper::OptCtlUncMinToy::uDim = 0;
std::vector<double> hyper::OptCtlUncMinToy::ctl;
std::vector<double> hyper::OptCtlUncMinToy::imCtl;
bool hyper::OptCtlUncMinToy::objeval = false;
double hyper::OptCtlUncMinToy::tf = 1.0;
double hyper::OptCtlUncMinToy::tint = 0.1;
std::vector<double> hyper::OptCtlUncMinToy::d;
std::vector<double> hyper::OptCtlUncMinToy::imd;
std::vector<double> hyper::OptCtlUncMinToy::tv;
std::vector<double> hyper::OptCtlUncMinToy::optu;
std::vector<double> hyper::OptCtlUncMinToy::optt;
int hyper::OptCtlUncMinToy::currGradIndex = 0;
double hyper::OptCtlUncMinToy::fObj = 1e6;
std::vector<double> hyper::OptCtlUncMinToy::x1;
std::vector<double> hyper::OptCtlUncMinToy::x2;
int hyper::OptCtlUncMinToy::N = 0;
std::vector<double> hyper::OptCtlUncMinToy::tph;
hyper::FirstOrderGPUSolv hyper::OptCtlUncMinToy::gpuSolv;

size_t pchipTest() {
	std::string Options("");
	PCHIPGPUTest pchipTester(Options);
	int now = GetTimeMs();
	try {
		pchipTester.testPCHIP();
		int later = GetTimeMs();
		printPassMessage("pchipTest", later-now);
		return 0;
	}
	catch ( std::runtime_error& re) {
		printFailMessage("pchipTest", re.what());
		return 1;
	}
}

size_t rkf45Test() {
	int now = GetTimeMs();
	try {
		RKF45OpenCLTest rkf45Test;
		rkf45Test.RKF45Test();
		int later = GetTimeMs();
		printPassMessage("rfk45Test", later-now);
		return 0;
	}
	catch ( std::runtime_error& re) {
		printFailMessage("rfk45Test", re.what());
		return 1;
	}
}


/**
 * Test the method in class VanDerPolGPU
 * void calcObjRK4GPU(std::vector<double>& ru, std::vector<double>& imu,  size_t Nt, size_t NGroups,
			           size_t NThreads, double Tf, double muMin, double muMax, double &reJ, double& imJ,
			           std::vector<std::vector<double>>& x1m, std::vector<std::vector<double>>& x2m,
			           bool getAllPts, double& g1R, double& g1I, double& g2R, double& g2I, bool useMonteCarloMu)
 */
void VanDerPolTest() {
	int now = GetTimeMs();
	try {
		size_t NThreads = 32;
		size_t NGroups = 32;
		size_t N = NThreads*NGroups;
		double Tf = 10.0;
		int Nt = (int) 1024*Tf; //Number of time intervals to keep h constant
		int Nc = 10000;
		std::vector<double> ru(N, 0.0);
		std::vector<double> imu(N, 0.0);
		std::string Options("-DNT=");
		Options += std::to_string(Nt);
		Options += " -DNC=" + Nc;
		hyper::VanDerPolGPU vdp(1e-6, Options);

		cout << "Options=" << Options << std::endl;
		double muMin = 0.0;
		double muMax = 1.0;
		double reJ, imJ;
		double g1R, g2R, g1I, g2I;
		std::vector<double> xinit(4);
		xinit[0]=3.0;
		xinit[1]=0.0;
		xinit[2]=0.0;
		xinit[3]=0.0;
		vdp.calcObjRK4GPU(ru, imu, Nt, Nc, NGroups, NThreads, Tf, muMin, muMax, xinit, reJ, imJ, g1R, g1I, g2R, g2I);
		//Extract the individual curves and plot them
		int later = GetTimeMs();
		printPassMessage("VanDerPolTest", later-now);
	}
	catch ( std::runtime_error& re) {
		printFailMessage("VanDerPolTest", re.what());
	}

}




void solveVanDerPolIpoptTest() {
	try {
		int now = GetTimeMs();
		SmartPtr<TNLP> nlp = new hyper::VanDerPolODE_NLP(128, 10000);
		// Create a new instance of IpoptApplication
		//  (use a SmartPtr, not raw)
		// We are using the factory, since this allows us to compile this
		// example with an Ipopt Windows DLL
		SmartPtr <IpoptApplication> app = IpoptApplicationFactory();
		app->RethrowNonIpoptException(true);


		app->Options()->SetStringValue("mu_strategy", "adaptive");
		app->Options()->SetStringValue("output_file", "ipopt.out");
		app->Options()->SetStringValue("hessian_approximation","limited-memory");
		app->Options()->SetStringValue("check_derivatives_for_naninf", "yes");
		app->Options()->SetIntegerValue("max_iter", 100000);
		app->Options()->SetNumericValue("tol", 1e-4);
		//app->Options()->SetStringValue("derivative_test", "first-order");
		//app->Options()->SetNumericValue("derivative_test_perturbation", 1e-8);
		// The following overwrites the default name (ipopt.opt) of the
		// options file
		// app->Options()->SetStringValue("option_file_name", "hs071.opt");

		// Initialize the IpoptApplication and process the options
		ApplicationReturnStatus status;
		status = app->Initialize();
		if (status != Solve_Succeeded) {
			std::cout << std::endl << std::endl
				<< "*** Error during initialization!" << std::endl;
		}


		// Ask Ipopt to solve the problem
		status = app->OptimizeTNLP(nlp);
		if (status == Solve_Succeeded || status == Solved_To_Acceptable_Level) {
			std::cout << std::endl << std::endl << "*** The problem solved!"
				<< std::endl;

		} else {
			std::cout << std::endl << std::endl << "*** The problem FAILED!"
				<< std::endl;

		}
		int later = GetTimeMs();
		printPassMessage("solveVanDerPolIpoptTest", later-now);

	}
	catch ( std::runtime_error& re) {
		printFailMessage("solveVanDerPolIpoptTest", re.what());

	}
}



/**
 * Test the mt19937_64 generator visually
 */
void RNGTest() {
	try {
		int now = GetTimeMs();
		std::random_device rd;
		std::mt19937_64 gen;
		double muSum=0.0;
		size_t testSize = 100000;
		std::vector<double> x(testSize);
		std::vector<double> y(testSize);
		/*std::srand(std::time(0));
			 int random_variable = std::rand();
			 for ( int i=0; i < numQuadPts; i++ ) {
				 mu[i]=muMin + 1.0*random_variable/RAND_MAX;
			 }*/

		std::uniform_real_distribution<double> dis(0.0, 1.0);
		for (int i = 0; i < testSize; i++) {
			x[i] = dis(gen);
			y[i] = dis(gen);
		}
		int later = GetTimeMs();
		printPassMessage("RNGTest pass", later-now);
		TApplication myapp("myapp",0,0);
		TCanvas c1("c1","random",2000,2000);
		c1.SetGrid();
		c1.cd();
		TGraph grR(x.size(), &x[0], &y[0]);
		grR.SetTitle("64 bit mt19937_64 random number Generator");
		grR.Draw("al");
		c1.Update();

		c1.Modified();
		myapp.Run();
	}
	catch ( std::runtime_error& re) {
		printFailMessage("RNGTest", re.what());

	}
}

void SaveLegendreZeros() {
	//Put the Legendre zeros here
	double x1024[512];
	double w1024[512];
	ofstream storeFile;
	std::string filename("/tmp/legzeros.txt");
	storeFile.open(filename);
	storeFile.precision(15);
	storeFile.setf(ios::fixed);
	storeFile.setf(ios::showpoint);
	for ( int i=511; i >=0; i-- ) {
		storeFile  << -1.0*x1024[i] << ",";
	}
	for ( int i=0; i < 512; i++ ) {
		storeFile  << x1024[i] << ",";
	}

	storeFile.close();
	ofstream storeFilew;
	std::string filenamew("/tmp/legweights.txt");
	storeFilew.open(filenamew);
	storeFilew.precision(15);
	storeFilew.setf(ios::fixed);
	storeFilew.setf(ios::showpoint);
	for ( int i=511; i >=0; i-- ) {
		storeFilew  << w1024[i] << ",";
	}
	for ( int i=0; i < 512; i++ ) {
		storeFilew  << w1024[i] << ",";
	}

	storeFilew.close();

}





int main(int argc, char* argv[]) {



	google::InitGoogleLogging(argv[0]);
	google::InstallFailureSignalHandler();
//	try {
//		serverTest();
//	} catch (std::runtime_error& re) {
//		printFailMessage("serverTest", re.what());
//	}
//	try {
//		solveUncToyProbIpopt();
//	} catch (std::runtime_error& re) {
//		printFailMessage("toyproblemIpoptTest", re.what());
//	}

	//pchipTest();
	//VanDerPolTest();
	//RNGTest();
	solveVanDerPolIpoptTest();
	//rkf45Test();
	//solveUncToyProbIpopt();
	//SaveLegendreZeros();

}

