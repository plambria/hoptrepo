#define LINEAR_MEM_ACCESS
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
#ifndef NDEBUG
#pragma OPENCL EXTENSION cl_amd_printf : enable
#endif

#define H 					1e-20
#define ERR_CONV			1
#define VDIM				4

#include "pchip.cl"


/**
 *
 */
__kernel void testPCHIP(__global double* 	x_g,  //read only
						__global double* 	f_g,  //read only
						__global double*    xe_g, //write
						__global double*	fe_g //write
						)  {
	/*  ************************************Original Test***********************************
	 * # define N 21
# define NE 101

  double d[N];
  double diff;
  double f[N];
  double fd[NE];
  double fe[NE];
  int i;
  double x[N];
  double xe[NE];

  printf ( "\n" );
  printf ( "TEST235\n" );
  printf ( "  SPLINE_PCHIP_SET sets up a piecewise cubic\n" );
  printf ( "    Hermite interpolant.\n" );
  printf ( "  SPLINE_PCHIP_VAL evaluates the interpolant.\n" );
  printf ( "\n" );
//
//  Compute Runge's function at N points in [-1,1].
//
  for ( i = 0; i < N; i++ )
  {
    x[i] = -1.0 + ( double ) ( i ) / 10.0;
    f[i] = frunge ( x[i] );
  }
//
//  SPLINE_PCHIP_SET takes the data in X and F, and constructs a table in D
//  that defines the interpolant.
//
  spline_pchip_set ( N, x, f, d );
//
//  Evaluate the interpolant and derivative at NE points from -1 to 0.
//
  for ( i = 0; i < NE; i++ )
  {
    xe[i] = -1.0 + ( double ) ( i ) / ( double ) ( NE - 1 );
  }

  spline_pchip_val ( N, x, f, d, NE, xe, fe );
//
//  Print the table of X, F(exact) and F(interpolated)
//
  for ( i = 0; i < NE; i++ )
  {
    diff = fe[i] - frunge ( xe[i] );

    printf ( "  %8g  %10g  %10g  %10g\n", xe[i], frunge ( xe[i] ), fe[i], diff );
  }

  return;
# undef N
# undef NE
	 *
	 */
  const size_t  N=21;
  const size_t NE=101;
  printf((__constant char *)"Entering testPCHIP\n");
  local double d[N];
  local double f[N];
  local double fe[NE];
  int i;
  local double x[N];
  local double xe[NE];

  size_t localId = get_local_id(0);
  size_t globalId = get_global_id(0);
  size_t groupId = get_group_id(0);
  size_t groupSize = get_local_size(0); //Number of local work items
  //There is no parallelization here so let a single work item to everything
  if ( groupId==0 && localId ==0 ) {
	  for ( int i=0; i < N; i++) {
		  x[i]=x_g[i];
		  f[i]=f_g[i];
	  }

//
//  SPLINE_PCHIP_SET takes the data in X and F, and constructs a table in D
//  that defines the interpolant.
//
	  spline_pchip_set ( N, x, f, d );
//
//  Evaluate the interpolant and derivative at NE points from -1 to 0.
//
	  for ( i = 0; i < NE; i++ ) {
		  xe[i] = -1.0 + ( double ) ( i ) / ( double ) ( NE - 1 );
	  }

	  spline_pchip_val ( N, x, f, d, NE, xe, fe );
	  for ( i = 0; i < NE; i++ ) {
		  //Copy to global memory
		  xe_g[i]=xe[i];
		  fe_g[i]=fe[i];

	  }


  }
  printf((__constant char *)"Exiting testPCHIP\n");
}
