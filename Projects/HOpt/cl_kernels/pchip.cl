#ifndef PCHIP_H_CL
#define PCHIP_H_CL


/*
  Purpose:

    CHFEV evaluates a cubic polynomial given in Hermite form.

  Discussion:

    This routine evaluates a cubic polynomial given in Hermite form at an
    array of points.  While designed for use by SPLINE_PCHIP_VAL, it may
    be useful directly as an evaluator for a piecewise cubic
    Hermite function in applications, such as graphing, where
    the interval is known in advance.

    The cubic polynomial is determined by function values
    F1, F2 and derivatives D1, D2 on the interval [X1,X2].

    Parameters:

    Input, double X1, X2, the endpoints of the interval of
    definition of the cubic.  X1 and X2 must be distinct.

    Input, double F1, F2, the values of the function at X1 and
    X2, respectively.

    Input, double D1, D2, the derivative values at X1 and
    X2, respectively.

    Input, int NE, the number of evaluation points.

    Input, double XE[NE], the points at which the function is to
    be evaluated.  If any of the XE are outside the interval
    [X1,X2], a warning error is returned in NEXT.

    Output, double FE[NE], the value of the cubic function
    at the points XE.

    Output, int NEXT[2], indicates the number of extrapolation points:
    NEXT[0] = number of evaluation points to the left of interval.
    NEXT[1] = number of evaluation points to the right of interval.

    Output, int CHFEV, error flag.
    0, no errors.
    -1, NE < 1.
    -2, X1 == X2.
*/
int chfev ( double x1, double x2, double f1, double f2, double d1, double d2,
  int ne, local double xe[], local double fe[], int next[] )

{
  double c2;
  double c3;
  double del1;
  double del2;
  double delta;
  double h;
  int i;
  double x;
  double xma;
  double xmi;


  h = x2 - x1;

 /*
  Initialize.
*/
  next[0] = 0;
  next[1] = 0;
  //xmi = r8_min ( 0.0, h );
  xmi = 0.0 < h ? 0.0 : h;
  //xma = r8_max ( 0.0, h );
  xma = 0.0 > h ? 0.0 : h;
/*
  Compute cubic coefficients expanded about X1.
*/
  delta = ( f2 - f1 ) / h;
  del1 = ( d1 - delta ) / h;
  del2 = ( d2 - delta ) / h;
  c2 = -( del1 + del1 + del2 );
  c3 = ( del1 + del2 ) / h;
/*
  Evaluation loop.
*/
  for ( i = 0; i < ne; i++ )
  {
    x = xe[i] - x1;
    fe[i] = f1 + x * ( d1 + x * ( c2 + x * c3 ) );
/*
  Count the extrapolation points.
*/
    if ( x < xmi )
    {
      next[0] = next[0] + 1;
    }
    else {
    	//This is dead code to allow GPU to optimize
    	double temp=next[0];
    }

    if ( xma < x )
    {
      next[1] = next[1] + 1;
    }
    else {
    	//This is dead code to allow GPU to optimize
    	double temp=next[1];
    }


  }

  return 0;
}

/**
 * Only use for N>=3
 * Normal routine safeguards have been removed
 */
int spline_pchip_set ( int n,  local double x[], local double f[], local double d[] )
{
  double del1;
  double del2;
  double dmax;
  double dmin;
  double drat1;
  double drat2;
  double dsave;
  double h1;
  double h2;
  double hsum;
  double hsumt3;
  int i;
  int ierr;
  int nless1;
  double temp;
  double w1;
  double w2;


  ierr = 0;
  nless1 = n - 1;
  h1 = x[1] - x[0];
  del1 = ( f[1] - f[0] ) / h1;
  dsave = del1;
/*
  Normal case, 3 <= N.
*/
  h2 = x[2] - x[1];
  del2 = ( f[2] - f[1] ) / h2;
/*
  Set D(1) via non-centered three point formula, adjusted to be
  shape preserving.
*/
  hsum = h1 + h2;
  w1 = ( h1 + hsum ) / hsum;
  w2 = -h1 / hsum;
  d[0] = w1 * del1 + w2 * del2;


  if ( d[0]*del1 <= 0.0) {
	  d[0] = 0.0;
  }
/*
  Need do this check only if monotonicity switches.
*/
  else if ( del1*del2  < 0.0 )
  {
     dmax = 3.0 * del1;

     if ( fabs ( dmax ) < fabs ( d[0] ) )
     {
       d[0] = dmax;
     }

  }
/*
  Loop through interior points.
*/
  for ( i = 2; i <= nless1; i++ )
  {
    if ( 2 < i )
    {
      h1 = h2;
      h2 = x[i] - x[i-1];
      hsum = h1 + h2;
      del1 = del2;
      del2 = ( f[i] - f[i-1] ) / h2;
    }
/*
  Set D(I)=0 unless data are strictly monotonic.
*/
    d[i-1] = 0.0;

    temp = del1*del2;

    if ( temp < 0.0 )
    {
      ierr = ierr + 1;
      dsave = del2;
    }
/*
  Count number of changes in direction of monotonicity.
*/
    else if ( temp == 0.0 )
    {
      if ( del2 != 0.0 )
      {
        if ( dsave * del2  < 0.0 )
        {
          ierr = ierr + 1;
        }
        dsave = del2;
      }
    }
/*
  Use Brodlie modification of Butland formula.
*/
    else
    {
      hsumt3 = 3.0 * hsum;
      w1 = ( hsum + h1 ) / hsumt3;
      w2 = ( hsum + h2 ) / hsumt3;
      //dmax = r8_max ( fabs ( del1 ), fabs ( del2 ) );
      double t1 = fabs(del1);
      double t2 = fabs(del2);
      dmax = t1 > t2 ? t1 : t2;
      //dmin = r8_min ( fabs ( del1 ), fabs ( del2 ) );
      dmin = t1 < t2 ? t1 : t2;
      drat1 = del1 / dmax;
      drat2 = del2 / dmax;
      d[i-1] = dmin / ( w1 * drat1 + w2 * drat2 );
    }
  }
/*
  Set D(N) via non-centered three point formula, adjusted to be
  shape preserving.
*/
  w1 = -h2 / hsum;
  w2 = ( h2 + hsum ) / hsum;
  d[n-1] = w1 * del1 + w2 * del2;

  if (  (d[n-1] * del2 ) <= 0.0 )
  {
    d[n-1] = 0.0;
  }
  else if (  ( del1*del2 ) < 0.0 )
  {
/*
  Need do this check only if monotonicity switches.
*/
    dmax = 3.0 * del2;

    if ( fabs ( dmax ) < fabs ( d[n-1] ) )
    {
      d[n-1] = dmax;
    }

  }
  return 0;
}


int spline_pchip_val ( int n, local double x[], local double f[], local double d[],
  int ne, local double xe[], local double fe[] )

//****************************************************************************80
//
//  Purpose:
//
//    SPLINE_PCHIP_VAL evaluates a piecewise cubic Hermite function.
//
//  Description:
//
//    This routine may be used by itself for Hermite interpolation, or as an
//    evaluator for SPLINE_PCHIP_SET.
//
//    This routine evaluates the cubic Hermite function at the points XE.
//
//    Most of the coding between the call to CHFEV and the end of
//    the IR loop could be eliminated if it were permissible to
//    assume that XE is ordered relative to X.
//
//    CHFEV does not assume that X1 is less than X2.  Thus, it would
//    be possible to write a version of SPLINE_PCHIP_VAL that assumes a strictly
//    decreasing X array by simply running the IR loop backwards
//    and reversing the order of appropriate tests.
//
//    The present code has a minor bug, which I have decided is not
//    worth the effort that would be required to fix it.
//    If XE contains points in [X(N-1),X(N)], followed by points less than
//    X(N-1), followed by points greater than X(N), the extrapolation points
//    will be counted (at least) twice in the total returned in IERR.
//
//    The evaluation will be most efficient if the elements of XE are
//    increasing relative to X; that is, for all J <= K,
//      X(I) <= XE(J)
//    implies
//      X(I) <= XE(K).
//
//    If any of the XE are outside the interval [X(1),X(N)],
//    values are extrapolated from the nearest extreme cubic,
//    and a warning error is returned.
//
//    This routine was originally named "PCHFE".
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    14 August 2005
//
//  Author:
//
//    Original FORTRAN77 version by Fred Fritsch, Lawrence Livermore National Laboratory.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Fred Fritsch, Ralph Carlson,
//    Monotone Piecewise Cubic Interpolation,
//    SIAM Journal on Numerical Analysis,
//    Volume 17, Number 2, April 1980, pages 238-246.
//
//  Parameters:
//
//    Input, int N, the number of data points.  N must be at least 2.
//
//    Input, double X[N], the strictly increasing independent
//    variable values.
//
//    Input, double F[N], the function values.
//
//    Input, double D[N], the derivative values.
//
//    Input, int NE, the number of evaluation points.
//
//    Input, double XE[NE], points at which the function is to
//    be evaluated.
//
//    Output, double FE[NE], the values of the cubic Hermite
//    function at XE.
//
{
  int i;
  int ierc;
  int ierr;
  int ir;
  int j;
  int j_first;
  int j_new;
  int j_save;
  int next[2];
  int nj;

  ierr = 0;
//
//  Loop over intervals.
//  The interval index is IL = IR-1.
//  The interval is X(IL) <= X < X(IR).
//
  j_first = 1;
  ir = 2;

  for ( ; ; )
  {
//
//  Skip out of the loop if have processed all evaluation points.
//
    if ( ne < j_first )
    {
      break;
    }
//
//  Locate all points in the interval.
//
    j_save = ne + 1;

    for ( j = j_first; j <= ne; j++ )
    {
      if ( x[ir-1] <= xe[j-1] )
      {
        j_save = j;
        if ( ir == n )
        {
          j_save = ne + 1;
        }
        break;
      }
    }
//
//  Have located first point beyond interval.
//
    j = j_save;

    nj = j - j_first;
//
//  Skip evaluation if no points in interval.
//
    if ( nj != 0 )
    {
//
//  Evaluate cubic at XE(J_FIRST:J-1).
//
      ierc = chfev ( x[ir-2], x[ir-1], f[ir-2], f[ir-1], d[ir-2], d[ir-1],
        nj, xe+j_first-1, fe+j_first-1, next );

      if ( ierc < 0 )
      {
        ierr = -5;
        return ierr;
      }
//
//  In the current set of XE points, there are NEXT(2) to the right of X(IR).
//
      if ( next[1] != 0 )
      {
        if ( ir < n )
        {
          ierr = -5;
          return ierr;
        }
//
//  These are actually extrapolation points.
//
        ierr = ierr + next[1];

      }
//
//  In the current set of XE points, there are NEXT(1) to the left of X(IR-1).
//
      if ( next[0] != 0 )
      {
//
//  These are actually extrapolation points.
//
        if ( ir <= 2 )
        {
          ierr = ierr + next[0];
        }
        else
        {
          j_new = -1;

          for ( i = j_first; i <= j-1; i++ )
          {
            if ( xe[i-1] < x[ir-2] )
            {
              j_new = i;
              break;
            }
          }

          if ( j_new == -1 )
          {
            ierr = -5;
            return ierr;
          }
//
//  Reset J.  This will be the new J_FIRST.
//
          j = j_new;
//
//  Now find out how far to back up in the X array.
//
          for ( i = 1; i <= ir-1; i++ )
          {
            if ( xe[j-1] < x[i-1] )
            {
              break;
            }
          }
//
//  At this point, either XE(J) < X(1) or X(i-1) <= XE(J) < X(I) .
//
//  Reset IR, recognizing that it will be incremented before cycling.
//
          ir =  1 >  i-1 ? 1 : i-1;
        }
      }

      j_first = j;
    }

    ir = ir + 1;

    if ( n < ir )
    {
      break;
    }

  }

  return 0;
}










#endif
