#define LINEAR_MEM_ACCESS
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
#ifndef NDEBUG
#pragma OPENCL EXTENSION cl_amd_printf : enable
#endif

#define H 					1e-20
#define ERR_CONV			1
#define VDIM				4
#define M_RAN_INVM64 5.42101086242752217003726400434970855e-20

#include <Random123.cl>

/**
 * Calculate the dynamics of the van Der Pol oscillator
 * @param x[in]-the x state
 * @param xp[out]- $\dot{x}$
 * @param mu[in]-$\mu$
 * @param ctrl[in]-the control ctrl to be used
 *
 */
void vanDerPolDyn(double* x, double* xp, double mu, double ctrl) {
	//We need to determine what the control is at time t-use  PCHIP

	xp[0] = x[1];

	xp[1] = mu * (1.0 - pow(x[0], 2)) * x[1] - x[0] + ctrl;

}

/**
 * Calculate the dynamics of the van Der Pol oscillator in the complex plane
 * @param x1[in]-the x1 state arrays as as real and imaginary
 * @param x2[in]-the x2 state array s as real and imaginary
 * @param xp1[out]- $\dot{x1}$ array as real and imaginary
 * @param xp2[out]- $\dot{x2}$ array as as real and imaginary
 * @param mu[in]-$\mu$-real
 * @param ctrl[in]-the control as real and imaginary
 *
 */
void vanDerPolDynComplx(double* x1, double *x2, double* xp1, double *xp2, double mu, double* ctrl) {
	xp1[0] = x2[0];
	xp1[1] = x2[1];
	xp2[0] = mu* (x2[0] - pow(x1[0], 2) * x2[0] + pow(x1[1], 2) * x2[0] + 2 * x1[0] * x1[1] * x2[1]) - x1[0] + ctrl[0];
	xp2[1] = mu* (-2.0 * x1[0] * x1[1] * x2[0] + x2[1] - x2[1] * pow(x1[0], 2)+ pow(x1[1], 2) * x2[1]) - x1[1] + ctrl[1];

}


// See uniform.hpp from the Random123 examples for the original source code
/*
 * template <typename Ftype, typename Itype>
 R123_CUDA_DEVICE R123_STATIC_INLINE Ftype uneg11(Itype in){
 typedef typename make_signed<Itype>::type Stype;
 R123_CONSTEXPR Ftype factor = Ftype(1.)/(maxTvalue<Stype>() + Ftype(1.));
 R123_CONSTEXPR Ftype halffactor = Ftype(0.5)*factor;
 #if R123_UNIFORM_FLOAT_STORE
 volatile Ftype x = Stype(in)*factor; return x+halffactor;
 #else
 return Stype(in)*factor + halffactor;
 #endif
 }
 *
 */

double Ran64Dbl(long lRan) {
	unsigned long ulRan = (unsigned long) lRan;
	double factor = 1.0 / (ULONG_MAX + 1.0);
	double halffactor = 0.5 * factor;
	double x = ulRan * factor;
	double d = x + halffactor;
	return d;
}

/**
 * Perform linear interpolation to retrieve the control at time t
 * The control dimension is NC passed in as a define from the CPU
 */
void linInterpCtrl(double* ctrl, __global double* rCtrl, __global double* imCtrl, double Tf, double t ) {
	double interv = Tf / (NC-1); //Here NC is passed in as a define
	int leftIndex = (int) (t /interv);
	double rem = t - (interv * leftIndex);

	//Perform linear interpolation
	if ( t < Tf ) {
		ctrl[0] = rCtrl[leftIndex] + (rem / interv)*(rCtrl[leftIndex+1]-rCtrl[leftIndex]);
		ctrl[1] = imCtrl[leftIndex] + (rem / interv)*(imCtrl[leftIndex+1]-imCtrl[leftIndex]);
	}
	else {
		ctrl[0] = rCtrl[NC-1];
		ctrl[1] = imCtrl[NC-1];
	}
}



//Fourth order Runge Kutta is as follows:
// $k_1=h f(t_n, x_n)$
// $k_2=h f(t_n+h/2, x_n+k_1/2*h)$
// $k_3=h f(t_n +h/2, x_n+k_2/2*h)$
// $k_4=h f(t_n+h, x_n + k_3*h)$
// $x_{n+1}=x_n+k_1/6+k_2/3+k_3/3+k_4/6 +O(h^5)$

void solveODERK4(double* x1, double* x2, double currMu,  __global double* rCtrl, __global double* imCtrl, __global double* xinit, double Tf) {
	size_t globalId = get_global_id(0);
	double xp1[2];
	double xp2[2];
	double k11[2];
	double k12[2];
	double k21[2];
	double k22[2];
	double k31[2];
	double k32[2];
	double k41[2];
	double k42[2];

	//Initial conditions
	x1[0]=xinit[0];
	x1[1]=xinit[1];
	x2[0]=xinit[2];
	x2[1]=xinit[3];

	double h=Tf/(1.0*NT-1.0);//Step size for RK4


	//printf((__constant char *)"kernel mu=%f\n", currMu);

	double ctrl[2];
	//Initial point
	for ( int i=0; i < NT-1; i++) { //Note the max value is NT-1 because it propagates to the next step
		double t = i*h;
		/*if ( 0 == globalId ) {
			printf("t=%12f \n", t);
		}*/
		//printf((__constant char *)"kernel ctrl=%f\n", ctrl[0]);
		linInterpCtrl(ctrl, rCtrl, imCtrl, Tf, t);
		vanDerPolDynComplx(x1, x2, xp1, xp2, currMu, ctrl);//Here we have xp
		/*if ( 0 == globalId ) {
			printf("ctrl[0]=%12f\n", ctrl[0]);
			printf("ctrl[1]=%12f\n", ctrl[1]);
		}*/
		k11[0]=xp1[0];
		k11[1]=xp1[1];
		k12[0]=xp2[0];
		k12[1]=xp2[1];

		double xTemp1[2];
		double xTemp2[2];
		xTemp1[0]= x1[0] + h*k11[0]/2.0;
		xTemp1[1]= x1[1] + h*k11[1]/2.0;
		xTemp2[0]= x2[0] + h*k12[0]/2.0;
		xTemp2[1]= x2[1] + h*k12[1]/2.0;
		t+=h/2; //Increment time by 1/2 step
		/*if ( 0 == globalId ) {
			printf("t=%12f \n", t);
		}*/

		linInterpCtrl(ctrl, rCtrl, imCtrl, Tf, t);
		/*if ( 0 == globalId ) {
			printf("ctrl[0]=%12f\n", ctrl[0]);
			printf("ctrl[1]=%12f\n", ctrl[1]);
		}*/

		vanDerPolDynComplx(xTemp1, xTemp2, xp1, xp2, currMu, ctrl);//Here we have xp

		k21[0]=xp1[0];
		k21[1]=xp1[1];
		k22[0]=xp2[0];
		k22[1]=xp2[1];

		xTemp1[0]= x1[0] + h*k21[0]/2.0;
		xTemp1[1]= x1[1] + h*k21[1]/2.0;
		xTemp2[0]= x2[0] + h*k22[0]/2.0;
		xTemp2[1]= x2[1] + h*k22[1]/2.0;
		vanDerPolDynComplx(xTemp1, xTemp2, xp1, xp2, currMu, ctrl);//Here we have xp

		k31[0]=xp1[0];
		k31[1]=xp1[1];
		k32[0]=xp2[0];
		k32[1]=xp2[1];

		xTemp1[0]= x1[0] + h*k31[0];
		xTemp1[1]= x1[1] + h*k31[1];
		xTemp2[0]= x2[0] + h*k32[0];
		xTemp2[1]= x2[1] + h*k32[1];
		t+=h/2;
		/*if ( 0 == globalId ) {
			printf("t=%12f \n", t);
		}*/

		linInterpCtrl(ctrl, rCtrl, imCtrl, Tf, t);

		vanDerPolDynComplx(xTemp1, xTemp2, xp1, xp2, currMu, ctrl);//Here we have xp

		k41[0]=xp1[0];
		k41[1]=xp1[1];
		k42[0]=xp2[0];
		k42[1]=xp2[1];

		x1[0]=x1[0]+(k11[0]/6+k21[0]/3+k31[0]/3+k41[0]/6)*h;
		x1[1]=x1[1]+(k11[1]/6+k21[1]/3+k31[1]/3+k41[1]/6)*h;

		x2[0]=x2[0]+(k12[0]/6+k22[0]/3+k32[0]/3+k42[0]/6)*h;
		x2[1]=x2[1]+(k12[1]/6+k22[1]/3+k32[1]/3+k42[1]/6)*h;

	}
	/*if ( 0 == globalId ) {
		printf("ctrl[0]=%12f\n", ctrl[0]);
		printf("ctrl[1]=%12f\n", ctrl[1]);
	}*/


}

/**
 *The purpose of this Kernel is to calculate the Objective Function by
 *integrating the Van Der Pol ODE in parallel for each mu
 *Note the NT is passed in as a define during compilation and is the dimension of mu and sumElem
 *The Quadrature based integration is of the form
 *$\int_a^b f(x)dx = \frac{b-a}{2} \Sum_{i=1}^{n} w_i f(\frac{b-a}{2}x \xi_i+\frac{b-a}{2})$
 *where $\xi_i are the n zeros of the nth degree Legendre polynomial $P_n(\xi)$
 *@param[in] rCtrl
 *@param[in] imCtrl
 *@param[in] w
 *@param[in] Tf
 *@param[in] mu
 *@param[in] muMin
 *@param[in] muMax
 *@param[out] rmuInt
 *@param[out] immmuInt
 *@param[out] rCtrlInt
 *@param[out] imCtrlInt
 *@param[out] scratch
 *@param[out] x1Out
 *@param[out] x2OUt
 */
__kernel void vanDerPolNLP_J(   __global double* 	rCtrl, //The real part of the control used for mu cost calculations spaced on even time intervals
								__global double* 	imCtrl,//The imaginary part of the control used for mu cost calculations spaced on even time intervals
								__global double*    quadTime,//Quadrature points on the t axis to match the weights for Gauss Legendre
								__global double* 	w,//The weight for calculation
								double 				Tf,//The final time
								__global double* 	mu,//The damping factor
								double 				muMin,//The minimum mu
								double 				muMax,//The maximum mu
								__global double* 	xinit,
								__global double* 	rmuInt,//Holds the individual results of the integral sum-dimension must be equal to number of groups
								__global double* 	immuInt,//Holds the individual results of the integral sum-dimension must be equal to number of groups
								__global double* 	rCtrlInt,//The real part of the control integral-dimension must be equal to number of groups
								__global double* 	imCtrlInt,//The imaginary part of the control integral-dimension must be equal to number of groups
								__global double* 	x1fR,//The real part of x1f
								__global double* 	x1fI,//The imaginary part of x1f
								__global double* 	x2fR,//The real part of x2f
								__global double* 	x2fI//The imaginary part of x2I
) {
	//printf((__constant char *)"Calling vanDerPolNLP_J with NT=%d\n", NT);
	//printf((__constant char *)"Calling vanDerPolRK4 with baseOffset=%lu\n", randomSeed);
	//printf((__constant char *)"Calling vanDerPolRK4 with CONTROL_SIZE=%d\n", CONTROL_SIZE);
	//For this implementation we should have M local elements running in parallel-Runge Kutta is serial for now

	size_t local_index = get_local_id(0);
	size_t globalId = get_global_id(0);
	size_t groupId = get_group_id(0);
	size_t groupSize = get_local_size(0);//Number of local work items
	size_t numGroups = get_num_groups(0);//Number of groups that will execute the Kernel

	//printf((__constant char *)"globalId=%lu\n", globalId);
	//printf((__constant char *)"kernel h=%10.15f\n", h);
	//printf((__constant char *)"kernel Tf=%f\n", Tf);
	//printf((__constant char *)"kernel NT=%d\n", NT);

	//Assume initial position at t=0 is at xinit
	double x1[2];
	x1[0]=xinit[0];
	x1[1]=xinit[1];
	double x2[2];
	x2[0]=xinit[2];
	x2[1]=xinit[3];
	double currMu=mu[globalId];
	solveODERK4(x1, x2, currMu,  rCtrl, imCtrl, xinit, Tf);
	//printf((__constant char *)"kernel x1f = %f\n", x1[0]);
	//printf((__constant char *)"kernel x2f = %f\n", x2[0]);
	/*if ( 0 == globalId ) {
		printf("Final x1[0]=%12f \n", x1[0]);
		printf("Final x1[1]=%12f \n", x1[1]);
		printf("Final x2[0]=%12f \n", x2[0]);
		printf("Final x2[1]=%12f \n", x2[1]);
	}*/
	x1fR[globalId]=x1[0];//Real part of x1
	x1fI[globalId]=x1[1];//Imaginary part of x1
	x2fR[globalId]=x2[0];//Real part of x2
	x2fI[globalId]=x2[1];//Imaginary part of x2
	//printf("x2fI = %12f\n", x2fI[globalId]);
	double A=(muMax-muMin)/2.0;
	double B=(muMax+muMin)/2.0;
	rmuInt[globalId] = A*w[globalId]*(pow(x1[0],2)-pow(x1[1], 2)+pow(x2[0],2)-pow(x2[1],2));
	immuInt[globalId] = A*w[globalId]*(2*x1[0]*x1[1]+2*x2[0]*x2[1]);

	double ctrl[2];
	linInterpCtrl(ctrl, rCtrl, imCtrl, Tf, quadTime[globalId]);

	rCtrlInt[globalId] = (Tf/2)*w[globalId]*(pow(ctrl[0], 2)-pow(ctrl[1], 2));
	imCtrlInt[globalId] = (Tf/2)*w[globalId]*(2*ctrl[0]*ctrl[1]);

}


__kernel void vanDerPolNLP_MC(   __global double* 	rCtrl, //The real part of the control used for mu cost calculations spaced on even time intervals
										 __global double* 	imCtrl,//The imaginary part of the control used for mu cost calculations spaced on even time intervals
										          double    Tf,
										 __global double* 	mu,//The damping factor-Assume there are NT of these
										 __global double* 	xinit,
										 __global double* 	x1fR,//The real part of x1f
										 __global double* 	x2fR//The real part of x2f
) {
	//printf((__constant char *)"Calling vanDerPolNLP_J with NT=%d\n", NT);
	//printf((__constant char *)"Calling vanDerPolRK4 with baseOffset=%lu\n", randomSeed);
	//printf((__constant char *)"Calling vanDerPolRK4 with CONTROL_SIZE=%d\n", CONTROL_SIZE);
	//For this implementation we should have M local elements running in parallel-Runge Kutta is serial for now

	size_t local_index = get_local_id(0);
	size_t globalId = get_global_id(0);
	size_t groupId = get_group_id(0);
	size_t groupSize = get_local_size(0);//Number of local work items
	size_t numGroups = get_num_groups(0);//Number of groups that will execute the Kernel

	//printf((__constant char *)"globalId=%lu\n", globalId);
	//printf((__constant char *)"kernel h=%10.15f\n", h);
	//printf((__constant char *)"kernel Tf=%f\n", Tf);
	//printf((__constant char *)"kernel NT=%d\n", NT);

	//Assume initial position at t=0 is at xinit
	double x1[2];
	x1[0]=xinit[0];
	x1[1]=xinit[1];
	double x2[2];
	x2[0]=xinit[2];
	x2[1]=xinit[3];
	double currMu=mu[globalId];
	solveODERK4(x1, x2, currMu,  rCtrl, imCtrl, xinit, Tf);
	//printf((__constant char *)"kernel x1f = %f\n", x1[0]);
	//printf((__constant char *)"kernel x2f = %f\n", x2[0]);
	/*if ( 0 == globalId ) {
		printf("Final x1[0]=%12f \n", x1[0]);
		printf("Final x1[1]=%12f \n", x1[1]);
		printf("Final x2[0]=%12f \n", x2[0]);
		printf("Final x2[1]=%12f \n", x2[1]);
	}*/
	x1fR[globalId]=x1[0];//Real part of x1
	x2fR[globalId]=x2[0];//Real part of x2
	//printf("x2fI = %12f\n", x2fI[globalId]);

}



