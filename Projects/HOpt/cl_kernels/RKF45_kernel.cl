/**********************************************************************

********************************************************************/

/*

 */

#define LINEAR_MEM_ACCESS
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
#ifndef NDEBUG
#pragma OPENCL EXTENSION cl_amd_printf : disable
#endif

#define H 					1e-20
#define ERR_CONV			1

int i4_max ( int i1, int i2 )

/******************************************************************************/
/*
  Purpose:

    I4_MAX returns the maximum of two I4's.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    29 August 2006

  Author:

    John Burkardt

  Parameters:

    Input, int I1, I2, are two integers to be compared.

    Output, int I4_MAX, the larger of I1 and I2.
*/
{
  int value;

  if ( i2 < i1 )
  {
    value = i1;
  }
  else
  {
    value = i2;
  }
  return value;
}


double r8_min ( double x, double y )
{
  double value;

  if ( y < x )
  {
    value = y;
  }
  else
  {
    value = x;
  }
  return value;
}

double r8_max ( double x, double y )
{
  double value;

  if ( y < x )
  {
    value = x;
  }
  else
  {
    value = y;
  }
  return value;
}

double r8_sign ( double x )
{
  double value;

  if ( x < 0.0 )
  {
    value = -1.0;
  }
  else
  {
    value = 1.0;
  }
  return value;
}



double pchst ( double arg1, double arg2 )

/******************************************************************************/
/*
  Purpose:

    PCHST: PCHIP sign-testing routine.

  Discussion:

    This routine essentially computes the sign of ARG1 * ARG2.

    The object is to do this without multiplying ARG1 * ARG2, to avoid
    possible over/underflow problems.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    12 August 2005

  Author:

    Original FORTRAN77 version by Fred Fritsch, Lawrence Livermore National Laboratory.
    C++ version by John Burkardt.

  Reference:

    Fred Fritsch, Ralph Carlson, 
    Monotone Piecewise Cubic Interpolation,
    SIAM Journal on Numerical Analysis,
    Volume 17, Number 2, April 1980, pages 238-246.

  Parameters:

    Input, double ARG1, ARG2, two values to check.

    Output, double PCHST,
    -1.0, if ARG1 and ARG2 are of opposite sign.
     0.0, if either argument is zero.
    +1.0, if ARG1 and ARG2 are of the same sign.
*/
{
  double value;

  if ( arg1 == 0.0 )
  {
    value = 0.0;
  }
  else if ( arg1 < 0.0 )
  {
    if ( arg2 < 0.0 )
    {
      value = 1.0;
    }
    else if ( arg2 == 0.0 )
    {
      value = 0.0;
    }
    else if ( 0.0 < arg2 )
    {
      value = -1.0;
    }
  }
  else if ( 0.0 < arg1 )
  {
    if ( arg2 < 0.0 )
    {
      value = -1.0;
    }
    else if ( arg2 == 0.0 )
    {
      value = 0.0;
    }
    else if ( 0.0 < arg2 )
    {
      value = 1.0;
    }
  }

  return value;
}


int chfev ( double x1, double x2, double f1, double f2, double d1, double d2,
  int ne, double xe[], double fe[], int next[] )

{
  double c2;
  double c3;
  double del1;
  double del2;
  double delta;
  double h;
  int i;
  int ierr;
  double x;
  double xma;
  double xmi;

  if ( ne < 1 )
  {
    ierr = -1;
    return ierr;
  }

  h = x2 - x1;

  if ( h == 0.0 )
  {
    ierr = -2;
    return ierr;
  }
/*
  Initialize.
*/
  ierr = 0;
  next[0] = 0;
  next[1] = 0;
  xmi = r8_min ( 0.0, h );
  xma = r8_max ( 0.0, h );
/*
  Compute cubic coefficients expanded about X1.
*/
  delta = ( f2 - f1 ) / h;
  del1 = ( d1 - delta ) / h;
  del2 = ( d2 - delta ) / h;
  c2 = -( del1 + del1 + del2 );
  c3 = ( del1 + del2 ) / h;
/*
  Evaluation loop.
*/
  for ( i = 0; i < ne; i++ )
  {
    x = xe[i] - x1;
    fe[i] = f1 + x * ( d1 + x * ( c2 + x * c3 ) );
/*
  Count the extrapolation points.
*/
    if ( x < xmi )
    {
      next[0] = next[0] + 1;
    }

    if ( xma < x )
    {
      next[1] = next[1] + 1;
    }

  }

  return 0;
}


int spline_pchip_set ( int n, local double x[], local double f[], local double d[] )
{
  double del1;
  double del2;
  double dmax;
  double dmin;
  double drat1;
  double drat2;
  double dsave;
  double h1;
  double h2;
  double hsum;
  double hsumt3;
  int i;
  int ierr;
  int nless1;
  double temp;
  double w1;
  double w2;
/*
  Check the arguments.
*/
  if ( n < 2 )
  {
    ierr = -1;
    return ierr;
  }

  for ( i = 1; i < n; i++ )
  {
    if ( x[i] <= x[i-1] )
    {
      ierr = -3;
      return ierr ;
    }
  }

  ierr = 0;
  nless1 = n - 1;
  h1 = x[1] - x[0];
  del1 = ( f[1] - f[0] ) / h1;
  dsave = del1;
/*
  Special case N=2, use linear interpolation.
*/
  if ( n == 2 )
  {
    d[0] = del1;
    d[n-1] = del1;
    return 0;
  }
/*
  Normal case, 3 <= N.
*/
  h2 = x[2] - x[1];
  del2 = ( f[2] - f[1] ) / h2;
/*
  Set D(1) via non-centered three point formula, adjusted to be
  shape preserving.
*/
  hsum = h1 + h2;
  w1 = ( h1 + hsum ) / hsum;
  w2 = -h1 / hsum;
  d[0] = w1 * del1 + w2 * del2;

  if ( pchst ( d[0], del1 ) <= 0.0 )
  {
    d[0] = 0.0;
  }
/*
  Need do this check only if monotonicity switches.
*/
  else if ( pchst ( del1, del2 ) < 0.0 )
  {
     dmax = 3.0 * del1;

     if ( fabs ( dmax ) < fabs ( d[0] ) )
     {
       d[0] = dmax;
     }

  }
/*
  Loop through interior points.
*/
  for ( i = 2; i <= nless1; i++ )
  {
    if ( 2 < i )
    {
      h1 = h2;
      h2 = x[i] - x[i-1];
      hsum = h1 + h2;
      del1 = del2;
      del2 = ( f[i] - f[i-1] ) / h2;
    }
/*
  Set D(I)=0 unless data are strictly monotonic.
*/
    d[i-1] = 0.0;

    temp = pchst ( del1, del2 );

    if ( temp < 0.0 )
    {
      ierr = ierr + 1;
      dsave = del2;
    }
/*
  Count number of changes in direction of monotonicity.
*/
    else if ( temp == 0.0 )
    {
      if ( del2 != 0.0 )
      {
        if ( pchst ( dsave, del2 ) < 0.0 )
        {
          ierr = ierr + 1;
        }
        dsave = del2;
      }
    }
/*
  Use Brodlie modification of Butland formula.
*/
    else
    {
      hsumt3 = 3.0 * hsum;
      w1 = ( hsum + h1 ) / hsumt3;
      w2 = ( hsum + h2 ) / hsumt3;
      dmax = r8_max ( fabs ( del1 ), fabs ( del2 ) );
      dmin = r8_min ( fabs ( del1 ), fabs ( del2 ) );
      drat1 = del1 / dmax;
      drat2 = del2 / dmax;
      d[i-1] = dmin / ( w1 * drat1 + w2 * drat2 );
    }
  }
/*
  Set D(N) via non-centered three point formula, adjusted to be
  shape preserving.
*/
  w1 = -h2 / hsum;
  w2 = ( h2 + hsum ) / hsum;
  d[n-1] = w1 * del1 + w2 * del2;

  if ( pchst ( d[n-1], del2 ) <= 0.0 )
  {
    d[n-1] = 0.0;
  }
  else if ( pchst ( del1, del2 ) < 0.0 )
  {
/*
  Need do this check only if monotonicity switches.
*/
    dmax = 3.0 * del2;

    if ( fabs ( dmax ) < fabs ( d[n-1] ) )
    {
      d[n-1] = dmax;
    }

  }
  return 0;
}

int spline_pchip_val ( int n, local double x[], local double f[], local double d[],
  int ne, double xe[], double fe[] )
{
  int i;
  int ierc;
  int ierr;
  int ir;
  int j;
  int j_first;
  int j_new;
  int j_save;
  int next[2];
  int nj;
/*
  Check arguments.
*/
  if ( n < 2 )
  {
    ierr = -1;
    return ierr;
  }

  for ( i = 1; i < n; i++ )
  {
    if ( x[i] <= x[i-1] )
    {
      ierr = -3;
      return ierr;
    }
  }

  if ( ne < 1 )
  {
    ierr = -4;
    return ierr;
  }

  ierr = 0;
/*
  Loop over intervals.
  The interval index is IL = IR-1.
  The interval is X(IL) <= X < X(IR).
*/
  j_first = 1;
  ir = 2;

  for ( ; ; )
  {
/*
  Skip out of the loop if have processed all evaluation points.
*/
    if ( ne < j_first )
    {
      break;
    }
/*
  Locate all points in the interval.
*/
    j_save = ne + 1;

    for ( j = j_first; j <= ne; j++ )
    {
      if ( x[ir-1] <= xe[j-1] )
      {
        j_save = j;
        if ( ir == n )
        {
          j_save = ne + 1;
        }
        break;
      }
    }
/*
  Have located first point beyond interval.
*/
    j = j_save;

    nj = j - j_first;
/*
  Skip evaluation if no points in interval.
*/
    if ( nj != 0 )
    {
/*
  Evaluate cubic at XE(J_FIRST:J-1).
*/
      ierc = chfev ( x[ir-2], x[ir-1], f[ir-2], f[ir-1], d[ir-2], d[ir-1], 
        nj, xe+j_first-1, fe+j_first-1, next );

      if ( ierc < 0 )
      {
        ierr = -5;
        return ierr;
      }
/*
  In the current set of XE points, there are NEXT(2) to the right of X(IR).
*/
      if ( next[1] != 0 )
      {
        if ( ir < n )
        {
          ierr = -5;
          return ierr;
        }
/*
  These are actually extrapolation points.
*/
        ierr = ierr + next[1];

      }
/*
  In the current set of XE points, there are NEXT(1) to the left of X(IR-1).
*/
      if ( next[0] != 0 )
      {
/*
  These are actually extrapolation points.
*/
        if ( ir <= 2 )
        {
          ierr = ierr + next[0];
        }
        else
        {
          j_new = -1;

          for ( i = j_first; i <= j-1; i++ )
          {
            if ( xe[i-1] < x[ir-2] )
            {
              j_new = i;
              break;
            }
          }

          if ( j_new == -1 )
          {
            ierr = -5;
            return ierr;
          }
/*
  Reset J.  This will be the new J_FIRST.
*/
          j = j_new;
/*
  Now find out how far to back up in the X array.
*/
          for ( i = 1; i <= ir-1; i++ )
          {
            if ( xe[j-1] < x[i-1] )
            {
              break;
            }
          }
/*
  At this point, either XE(J) < X(1) or X(i-1) <= XE(J) < X(I) .

  Reset IR, recognizing that it will be incremented before cycling.
*/
          ir = i4_max ( 1, i-1 );
        }
      }

      j_first = j;
    }

    ir = ir + 1;

    if ( n < ir )
    {
      break;
    }

  }

  return 0;
}

double r8_abs ( double x )

/******************************************************************************/
/*
  Purpose:

    R8_ABS returns the absolute value of an R8.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    07 May 2006

  Author:

    John Burkardt

  Parameters:

    Input, double X, the quantity whose absolute value is desired.

    Output, double R8_ABS, the absolute value of X.
*/
{
  double value;

  if ( 0.0 <= x )
  {
    value = x;
  } 
  else
  {
    value = - x;
  }
  return value;
}



/**
 * Function evaluates the dynamics at point t given x-here x is the control u
 */
void dynFComplex(double t, double x[], double xp[], local double ctl[], local double imctl[], local double tv[], local double d[], local double imd[], const int n) {
	//We need to find u based on t-use pchip interpolation
	double pu[1];
	double tt[1];
	tt[0]=t;
	spline_pchip_val ( n, tv, ctl, d, 1, tt, pu );
	//Now we have the real part of u
	double u=pu[0];
    //Let find the imaginary part
	double iu[1];
	spline_pchip_val ( n, tv, imctl, imd, 1,
			     tt, iu );
	double imU=iu[0];
	xp[0]=x[2];
	xp[1]=x[3];
	xp[2]=x[2] + u;
	xp[3]=x[3] + imU;

}


void r8_fehl ( double y[], double t, double h, double yp[], double f1[], double f2[], 
  double f3[], double f4[], double f5[], double s[], local double ctl[], local double imctl[], local double tv[], local double d[], local double imd[], const int n )
{
  int neqn=4;
  double ch;
  int i;

  ch = h / 4.0;

  for ( i = 0; i < neqn; i++ )
  {
    f5[i] = y[i] + ch * yp[i];
  }

  dynFComplex ( t + ch, f5, f1, ctl, imctl, tv, d, imd, n );

  ch = 3.0 * h / 32.0;

  for ( i = 0; i < neqn; i++ )
  {
    f5[i] = y[i] + ch * ( yp[i] + 3.0 * f1[i] );
  }

  dynFComplex ( t + 3.0 * h / 8.0, f5, f2, ctl, imctl, tv, d, imd, n  );

  ch = h / 2197.0;

  for ( i = 0; i < neqn; i++ )
  {
    f5[i] = y[i] + ch * 
    ( 1932.0 * yp[i] 
    + ( 7296.0 * f2[i] - 7200.0 * f1[i] ) 
    );
  }
//dynFComplex(double t, double x[], double xp[], double ctl[], double imctl[], double tv[], double d[], double imd[], const int n)
  dynFComplex ( t + 12.0 * h / 13.0, f5, f3, ctl, imctl, tv, d, imd, n );

  ch = h / 4104.0;

  for ( i = 0; i < neqn; i++ )
  {
    f5[i] = y[i] + ch * 
    ( 
      ( 8341.0 * yp[i] - 845.0 * f3[i] ) 
    + ( 29440.0 * f2[i] - 32832.0 * f1[i] ) 
    );
  }

  dynFComplex ( t + h, f5, f4 , ctl, imctl, tv, d, imd, n);

  ch = h / 20520.0;

  for ( i = 0; i < neqn; i++ )
  {
    f1[i] = y[i] + ch * 
    ( 
      ( -6080.0 * yp[i] 
      + ( 9295.0 * f3[i] - 5643.0 * f4[i] ) 
      ) 
    + ( 41040.0 * f1[i] - 28352.0 * f2[i] ) 
    );
  }

  dynFComplex ( t + h / 2.0, f1, f5, ctl, imctl, tv, d, imd, n );
/*
  Ready to compute the approximate solution at T+H.
*/
  ch = h / 7618050.0;

  for ( i = 0; i < neqn; i++ )
  {
    s[i] = y[i] + ch * 
    ( 
      ( 902880.0 * yp[i] 
      + ( 3855735.0 * f3[i] - 1371249.0 * f4[i] ) ) 
    + ( 3953664.0 * f2[i] + 277020.0 * f5[i] ) 
    );
  }

  return;
}

double r8_epsilon ( void )
{
  double r;

  r = 1.0;

  while ( 1.0 < ( double ) ( 1.0 + r )  )
  {
    r = r / 2.0;
  }
  r = 2.0 * r;

  return r;
}

//dynFComplex(double t, double x[], double xp[], double ctl[], double imctl[], double tv[], double d[], double imd[], const int n) 
int r8_rkf45 ( double y[], double yp[], double *t, double tout, double *relerr, 
               double abserr, int flag, local double ctl[], local double imctl[],
               local double tv[], local double d[], local double imd[], const int n,
               double* abserr_save, int* flag_save, double* h,
               int* init, int* kflag, int* kop, int* nfe, double* relerr_save, double*  remin)
{
# define MAXNFE 3000
# define neqn   4
  double ae;
  double dt;
  double ee;
  double eeoet;
  double eps;
  double esttol;
  double et;
  double f1[neqn];
  double f2[neqn];
  double f3[neqn];
  double f4[neqn];
  double f5[neqn];
  int flag_return;
  int hfaild;
  double hmin;
  int i;
  int k;
  int mflag;
  int output;
  double relerr_min;
  double s;
  double scale;
  double tol;
  double toln;
  double ypk;
  flag_return = flag;


  eps = r8_epsilon ( );
/*
  Check the input parameters.
*/

  if ( neqn < 1 )
  {
    flag_return = 8;
    return flag_return;
  }

  if ( (*relerr) < 0.0 )
  {
    flag_return = 8;
    return flag_return;
  }

  if ( abserr < 0.0 )
  {
    flag_return = 8;
    return flag_return;
  }

  if ( flag_return == 0 || 8 < flag_return  || flag_return < -2 )
  {
    flag_return = 8;
    return flag_return;
  }

  mflag = abs ( flag_return );
/*
  Is this a continuation call?
*/
  if ( mflag != 1 )
  {
    if ( *t == tout && *kflag != 3 )
    {
      flag_return = 8;
      return flag_return;
    }
/*
  FLAG = -2 or +2:
*/
    if ( mflag == 2 )
    {
      if ( *kflag == 3 )
      {
        flag_return = *flag_save;
        mflag = abs ( flag_return );
      }
      else if ( *init == 0 )
      {
        flag_return = *flag_save;
      }
      else if ( *kflag == 4 )
      {
        *nfe = 0;
      }
      else if ( *kflag == 5 && abserr == 0.0 )
      {
        /*fprintf ( stderr, "\n" );
        fprintf ( stderr, "R8_RKF45 - Fatal error!\n" );
        fprintf ( stderr, "  KFLAG = 5 and ABSERR = 0.0.\n" );*/
    	return 9;

      }
      else if ( *kflag == 6 && (*relerr) <= *relerr_save && abserr <= *abserr_save )
      {
        /*fprintf ( stderr, "\n" );
        fprintf ( stderr, "R8_RKF45 - Fatal error!\n" );
        fprintf ( stderr, "  KFLAG = 6 and\n" );
        fprintf ( stderr, "  RELERR <= RELERR_SAVE and\n" );
        fprintf ( stderr, "  ABSERR <= ABSERR_SAVE.\n" );*/
        return 9;
      }
    }
/*
  FLAG = 3, 4, 5, 6, 7 or 8.
*/
    else
    {
      if ( flag_return == 3 )
      {
        flag_return = *flag_save;
        if ( *kflag == 3 )
        {
          mflag = abs ( flag_return );
        }
      }
      else if ( flag_return == 4 )
      {
        *nfe = 0;
        flag_return = *flag_save;
        if ( *kflag == 3 )
        {
          mflag = abs ( flag_return );
        }
      }
      else if ( flag_return == 5 && 0.0 < abserr )
      {
        flag_return = *flag_save;
        if ( *kflag == 3 )
        {
          mflag = abs ( flag_return );
        }
      }
/*
  Integration cannot be continued because the user did not respond to
  the instructions pertaining to FLAG = 5, 6, 7 or 8.
*/
      else
      {
        /*fprintf ( stderr, "\n" );
        fprintf ( stderr, "R4_RKF45 - Fatal error!\n" );
        fprintf ( stderr, "  Integration cannot be continued.\n" );
        fprintf ( stderr, "  The user did not respond to the output value\n" );
        fprintf ( stderr, "  FLAG = 5, 6, 7 or 8.\n" );*/
        return 9;
      }
    }
  }
  //zero out the f elements
  for ( int i=0; i < neqn; i++ ) {
	  f1[i]=0.0;
	  f2[i]=0.0;
	  f3[i]=0.0;
	  f4[i]=0.0;
	  f5[i]=0.0;
  }
/*
  Save the input value of FLAG.  
  Set the continuation flag KFLAG for subsequent input checking.
*/
  *flag_save = flag_return;
  *kflag = 0;
/*
  Save RELERR and ABSERR for checking input on subsequent calls.
*/
  *relerr_save = (*relerr);
  *abserr_save = abserr;
/*
  Restrict the relative error tolerance to be at least 

    2*EPS+REMIN 

  to avoid limiting precision difficulties arising from impossible 
  accuracy requests.
*/
  relerr_min = 2.0 * r8_epsilon ( ) + *remin;
/*
  Is the relative error tolerance too small?
*/
  if ( (*relerr) < relerr_min )
  {
    (*relerr) = relerr_min;
    *kflag = 3;
    flag_return = 3;
    return flag_return;
  }

  dt = tout - *t;
/*
  Initialization:

  Set the initialization completion indicator, INIT;
  set the indicator for too many output points, KOP;
  evaluate the initial derivatives
  set the counter for function evaluations, NFE;
  estimate the starting stepsize.
*/

  if ( mflag == 1 )
  {
    *init = 0;
    *kop = 0;
    dynFComplex ( *t, y, yp , ctl, imctl, tv, d, imd, n);
    *nfe = 1;

    if ( *t == tout )
    {
      flag_return = 2;
      return flag_return;
    }

  }

  if ( *init == 0 )
  {
    *init = 1;
    *h = r8_abs ( dt );
    toln = 0.0;

    for ( k = 0; k < neqn; k++ )
    {
      tol = (*relerr) * r8_abs ( y[k] ) + abserr;
      if ( 0.0 < tol )
      {
        toln = tol;
        ypk = r8_abs ( yp[k] );
        if ( tol < ypk * pow ( *h, 5 ) )
        {
          *h = pow ( ( tol / ypk ), 0.2 );
        }
      }
    }

    if ( toln <= 0.0 )
    {
      *h = 0.0;
    }

    *h = r8_max ( *h, 26.0 * eps * r8_max ( r8_abs ( *t ), r8_abs ( dt ) ) );

    if ( flag_return < 0 )
    {
      *flag_save = -2;
    }
    else
    {
      *flag_save = 2;
    }
  }
/*
  Set stepsize for integration in the direction from T to TOUT.
*/
  *h = r8_sign ( dt ) * r8_abs ( *h );
/*
  Test to see if too may output points are being requested.
*/
  if ( 2.0 * r8_abs ( dt ) <= r8_abs ( *h ) )
  {
    *kop = *kop + 1;
  }
/*
  Unnecessary frequency of output.
*/
  if ( *kop == 100 )
  {
    *kop = 0;
    flag_return = 7;
    return flag_return;
  }
/*
  If we are too close to the output point, then simply extrapolate and return.
*/
  if ( r8_abs ( dt ) <= 26.0 * eps * r8_abs ( *t ) )
  {
    *t = tout;
    for ( i = 0; i < neqn; i++ )
    {
      y[i] = y[i] + dt * yp[i];
    }
    dynFComplex ( *t, y, yp , ctl, imctl, tv, d, imd, n);
    *nfe = *nfe + 1;

    flag_return = 2;
    return flag_return;
  }
/*
  Initialize the output point indicator.
*/
  output = 0;
/*
  To avoid premature underflow in the error tolerance function,
  scale the error tolerances.
*/
  scale = 2.0 / (*relerr);
  ae = scale * abserr;
/*
  Step by step integration.
*/
  for ( ; ; )
  {
    hfaild = 0;
/*
  Set the smallest allowable stepsize.
*/
    hmin = 26.0 * eps * r8_abs ( *t );
/*
  Adjust the stepsize if necessary to hit the output point.

  Look ahead two steps to avoid drastic changes in the stepsize and
  thus lessen the impact of output points on the code.
*/
    dt = tout - *t;

    if ( 2.0 * r8_abs ( *h ) <= r8_abs ( dt ) )
    {
    }
    else
/*
  Will the next successful step complete the integration to the output point?
*/
    {
      if ( r8_abs ( dt ) <= r8_abs ( *h ) )
      {
        output = 1;
        *h = dt;
      }
      else
      {
        *h = 0.5 * dt;
      }

    }
/*
  Here begins the core integrator for taking a single step.

  The tolerances have been scaled to avoid premature underflow in
  computing the error tolerance function ET.
  To avoid problems with zero crossings, relative error is measured
  using the average of the magnitudes of the solution at the
  beginning and end of a step.
  The error estimate formula has been grouped to control loss of
  significance.

  To distinguish the various arguments, H is not permitted
  to become smaller than 26 units of roundoff in T.
  Practical limits on the change in the stepsize are enforced to
  smooth the stepsize selection process and to avoid excessive
  chattering on problems having discontinuities.
  To prevent unnecessary failures, the code uses 9/10 the stepsize
  it estimates will succeed.

  After a step failure, the stepsize is not allowed to increase for
  the next attempted step.  This makes the code more efficient on
  problems having discontinuities and more effective in general
  since local extrapolation is being used and extra caution seems
  warranted.

  Test the number of derivative function evaluations.
  If okay, try to advance the integration from T to T+H.
*/
    for ( ; ; )
    {
/*
  Have we done too much work?
*/
      if ( MAXNFE < *nfe )
      {
        *kflag = 4;
        flag_return = 4;
        return flag_return;
      }
/*
  Advance an approximate solution over one step of length H.
*/

      r8_fehl ( y, *t, *h, yp, f1, f2, f3, f4, f5, f1, ctl, imctl, tv, d, imd, n );
      *nfe = *nfe + 5;
/*
  Compute and test allowable tolerances versus local error estimates
  and remove scaling of tolerances.  The relative error is
  measured with respect to the average of the magnitudes of the
  solution at the beginning and end of the step.
*/
      eeoet = 0.0;
 
      for ( k = 0; k < neqn; k++ )
      {
        et = r8_abs ( y[k] ) + r8_abs ( f1[k] ) + ae;

        if ( et <= 0.0 )
        {
          flag_return = 5;
          return flag_return;
        }

        ee = r8_abs 
        ( ( -2090.0 * yp[k] 
          + ( 21970.0 * f3[k] - 15048.0 * f4[k] ) 
          ) 
        + ( 22528.0 * f2[k] - 27360.0 * f5[k] ) 
        );

        eeoet = r8_max ( eeoet, ee / et );

      }

      esttol = r8_abs ( *h ) * eeoet * scale / 752400.0;
      if ( esttol <= 1.0 )
      {
        break;
      }
/*
  Unsuccessful step.  Reduce the stepsize, try again.
  The decrease is limited to a factor of 1/10.
*/
      hfaild = 1;
      output = 0;

      if ( esttol < 59049.0 )
      {
        s = 0.9 / pow ( esttol, 0.2 );
      }
      else
      {
        s = 0.1;
      }

      *h = s * (*h);

      if ( r8_abs ( *h ) < hmin )
      {
        *kflag = 6;
        flag_return = 6;
        return flag_return;
      }

    }
/*
  We exited the loop because we took a successful step.  
  Store the solution for T+H, and evaluate the derivative there.
*/
    *t = *t + (*h);

    for ( i = 0; i < neqn; i++ )
    {
      y[i] = f1[i];
    }
    dynFComplex ( *t, y, yp , ctl, imctl, tv, d, imd, n);
    *nfe = *nfe + 1;
/*
  Choose the next stepsize.  The increase is limited to a factor of 5.
  If the step failed, the next stepsize is not allowed to increase.
*/
    if ( 0.0001889568 < esttol )
    {
      s = 0.9 / pow ( esttol, 0.2 );
    }
    else
    {
      s = 5.0;
    }

    if ( hfaild )
    {
      s = r8_min ( s, 1.0 );
    }

    *h = r8_sign ( *h ) * r8_max ( s * r8_abs ( *h ), hmin );
/*
  End of core integrator

  Should we take another step?
*/
    if ( output != 0 )
    {

      *t = tout;
      flag_return = 2;
    }

    if ( flag_return < 1 )  //was <=0
    {
      flag_return = -2;
    }
    return flag_return;


  }
# undef MAXNFE

}






/** This test kernel assumes a single workgroup with a single work item
 * with theta =0. The integration time is from 0 to 1.0
 * The output array contains x1f_R, x1f_I, x2f_R, x2f_I
 * in positions x[0], x[1], x[2], x[3] respectively
 */
__kernel void testRKF45(const __global double* u,
		const __global double* imu,
		const __global double* theta,
		__global double* xout) {
#define N 128
	double x[4];
	x[0]=-1.00;
	x[1]=0.0;
	x[2]=1.50+theta[0];
	x[3]=0.0;
	double xp[4];
	local double ctl[N];
	local double tv[N];
	local double imCtl[N];

	for ( int i=0; i < N; i++ ) {
		ctl[i] = u[i];
		imCtl[i] = imu[i];
		tv[i]=(1.0*i)/(N-1);
	}

	//The parameters below are taken straight out of the provided examples
	double abserr = sqrt ( r8_epsilon ( ) );
	double relerr = sqrt ( r8_epsilon ( ) );

	int flag = -1;//Single step mode.  Let the routine figure out h

	double t_start = 0.0;
	double t_stop = 1.0;

	int n_step = 1;//Make the number of steps the same to correspond to the same values as tv vector

	double t = 0.0;
	double t_out = 0.0;
	local double d[N];
	local double imd[N];
	spline_pchip_set ( N, tv, ctl, d );
	spline_pchip_set ( N, tv, imCtl, imd );
	//Initialize
	dynFComplex ( t, x, xp , ctl, imCtl, tv, d, imd, N);

	int actNumSteps=0;
	/***********************************************
	 * The following block of variables are needed by r8_rfk45
	 */
	double abserr_save = -1.0;
	int flag_save = -1000;
	double h = -1.0;
	int init = -1000;
	int kflag = -1000;
	int kop = -1;
	int nfe = -1;
	double relerr_save = -1.0;
	double remin = 1.0E-12;

	for ( unsigned i_step = 1; i_step <= n_step; i_step++ )
	{
		t = ( ( double ) ( n_step - i_step + 1 ) * t_start
				+ ( double ) ( i_step - 1 ) * t_stop )
		/ ( double ) ( n_step );

		t_out = ( ( double ) ( n_step - i_step ) * t_start
				+ ( double ) ( i_step ) * t_stop )
		/ ( double ) ( n_step );
		actNumSteps++;
		while ( flag < 0 ) {

			flag = r8_rkf45 ( x, xp, &t, t_out, &relerr, abserr, flag, ctl, imCtl, tv, d, imd, N,
					&abserr_save, &flag_save, &h, &init, &kflag, &kop, &nfe, &relerr_save, &remin);

		       if ( flag == 4 ) { //Try again
	  		       flag = r8_rkf45 ( x, xp, &t, t_out, &relerr, abserr, flag, ctl, imctl, tv, d, imd, n,
	  		    		             &abserr_save, &flag_save, &h,  &init, &kflag, &kop, &nfe, &relerr_save, &remin);

		       }

		}
		if (flag==2 || flag == 3) { //We've reached the desired point
			flag=-2;//Set the flag for next step
		}
		else if ( flag == 4 ) {
		    continue; //leave flag as 4 in this case and try again-see function documentation
		}
		else if ( flag == 7 ) {
			flag = -2;
		}
		else {
			break; //Here if we had an error it means we reached some kind of edge point for our problem
		}

	}
	if ( (t_out < t_stop) || flag > 4) { //This means we had some kind of error
		printf((__constant char *)"Error during invoking RFK45 return flag=%d\n", flag);
	}


	//This is the imaginary part of $\frac{\partial{J}}{\partial{\theta}}
	xout[0]=x[0];
	xout[1]=x[1];
	xout[2]=x[2];
	xout[3]=x[3];
}
