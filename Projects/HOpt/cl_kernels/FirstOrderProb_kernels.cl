#define LINEAR_MEM_ACCESS
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
#ifndef NDEBUG
#pragma OPENCL EXTENSION cl_amd_printf : enable
#endif

#define H 					1e-20
#define ERR_CONV			1
#define VDIM				4

int i4_max ( int i1, int i2 )

/******************************************************************************/
/*
  Purpose:

    I4_MAX returns the maximum of two I4's.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    29 August 2006

  Author:

    John Burkardt

  Parameters:

    Input, int I1, I2, are two integers to be compared.

    Output, int I4_MAX, the larger of I1 and I2.
*/
{
  int value;

  if ( i2 < i1 )
  {
    value = i1;
  }
  else
  {
    value = i2;
  }
  return value;
}


double r8_min ( double x, double y )
{
  double value;

  if ( y < x )
  {
    value = y;
  }
  else
  {
    value = x;
  }
  return value;
}

double r8_max ( double x, double y )
{
  double value;

  if ( y < x )
  {
    value = x;
  }
  else
  {
    value = y;
  }
  return value;
}

double r8_sign ( double x )
{
  double value;

  if ( x < 0.0 )
  {
    value = -1.0;
  }
  else
  {
    value = 1.0;
  }
  return value;
}



double pchst ( double arg1, double arg2 )

/******************************************************************************/
/*
  Purpose:

    PCHST: PCHIP sign-testing routine.

  Discussion:

    This routine essentially computes the sign of ARG1 * ARG2.

    The object is to do this without multiplying ARG1 * ARG2, to avoid
    possible over/underflow problems.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    12 August 2005

  Author:

    Original FORTRAN77 version by Fred Fritsch, Lawrence Livermore National Laboratory.
    C++ version by John Burkardt.

  Reference:

    Fred Fritsch, Ralph Carlson, 
    Monotone Piecewise Cubic Interpolation,
    SIAM Journal on Numerical Analysis,
    Volume 17, Number 2, April 1980, pages 238-246.

  Parameters:

    Input, double ARG1, ARG2, two values to check.

    Output, double PCHST,
    -1.0, if ARG1 and ARG2 are of opposite sign.
     0.0, if either argument is zero.
    +1.0, if ARG1 and ARG2 are of the same sign.
*/
{
  double value;

  if ( arg1 == 0.0 )
  {
    value = 0.0;
  }
  else if ( arg1 < 0.0 )
  {
    if ( arg2 < 0.0 )
    {
      value = 1.0;
    }
    else if ( arg2 == 0.0 )
    {
      value = 0.0;
    }
    else if ( 0.0 < arg2 )
    {
      value = -1.0;
    }
  }
  else if ( 0.0 < arg1 )
  {
    if ( arg2 < 0.0 )
    {
      value = -1.0;
    }
    else if ( arg2 == 0.0 )
    {
      value = 0.0;
    }
    else if ( 0.0 < arg2 )
    {
      value = 1.0;
    }
  }

  return value;
}


/*
  Purpose:

    CHFEV evaluates a cubic polynomial given in Hermite form.

  Discussion:

    This routine evaluates a cubic polynomial given in Hermite form at an
    array of points.  While designed for use by SPLINE_PCHIP_VAL, it may
    be useful directly as an evaluator for a piecewise cubic
    Hermite function in applications, such as graphing, where
    the interval is known in advance.

    The cubic polynomial is determined by function values
    F1, F2 and derivatives D1, D2 on the interval [X1,X2].

    Parameters:

    Input, double X1, X2, the endpoints of the interval of
    definition of the cubic.  X1 and X2 must be distinct.

    Input, double F1, F2, the values of the function at X1 and
    X2, respectively.

    Input, double D1, D2, the derivative values at X1 and
    X2, respectively.

    Input, int NE, the number of evaluation points.

    Input, double XE[NE], the points at which the function is to
    be evaluated.  If any of the XE are outside the interval
    [X1,X2], a warning error is returned in NEXT.

    Output, double FE[NE], the value of the cubic function
    at the points XE.

    Output, int NEXT[2], indicates the number of extrapolation points:
    NEXT[0] = number of evaluation points to the left of interval.
    NEXT[1] = number of evaluation points to the right of interval.

    Output, int CHFEV, error flag.
    0, no errors.
    -1, NE < 1.
    -2, X1 == X2.
*/
int chfev ( double x1, double x2, double f1, double f2, double d1, double d2,
  int ne, double xe[], double fe[], int next[] )

{
  double c2;
  double c3;
  double del1;
  double del2;
  double delta;
  double h;
  int i;
  int ierr;
  double x;
  double xma;
  double xmi;

  if ( ne < 1 )
  {
    ierr = -1;
    return ierr;
  }

  h = x2 - x1;

  if ( h == 0.0 )
  {
    ierr = -2;
    return ierr;
  }
/*
  Initialize.
*/
  ierr = 0;
  next[0] = 0;
  next[1] = 0;
  xmi = r8_min ( 0.0, h );
  xma = r8_max ( 0.0, h );
/*
  Compute cubic coefficients expanded about X1.
*/
  delta = ( f2 - f1 ) / h;
  del1 = ( d1 - delta ) / h;
  del2 = ( d2 - delta ) / h;
  c2 = -( del1 + del1 + del2 );
  c3 = ( del1 + del2 ) / h;
/*
  Evaluation loop.
*/
  for ( i = 0; i < ne; i++ )
  {
    x = xe[i] - x1;
    fe[i] = f1 + x * ( d1 + x * ( c2 + x * c3 ) );
/*
  Count the extrapolation points.
*/
    if ( x < xmi )
    {
      next[0] = next[0] + 1;
    }

    if ( xma < x )
    {
      next[1] = next[1] + 1;
    }

  }

  return 0;
}


int spline_pchip_set ( int n, local double x[], local double f[], local double d[] )
{
  double del1;
  double del2;
  double dmax;
  double dmin;
  double drat1;
  double drat2;
  double dsave;
  double h1;
  double h2;
  double hsum;
  double hsumt3;
  int i;
  int ierr;
  int nless1;
  double temp;
  double w1;
  double w2;
/*
  Check the arguments.
*/
  if ( n < 2 )
  {
    ierr = -1;
    return ierr;
  }

  for ( i = 1; i < n; i++ )
  {
    if ( x[i] <= x[i-1] )
    {
      ierr = -3;
      return ierr ;
    }
  }

  ierr = 0;
  nless1 = n - 1;
  h1 = x[1] - x[0];
  del1 = ( f[1] - f[0] ) / h1;
  dsave = del1;
/*
  Special case N=2, use linear interpolation.
*/
  if ( n == 2 )
  {
    d[0] = del1;
    d[n-1] = del1;
    return 0;
  }
/*
  Normal case, 3 <= N.
*/
  h2 = x[2] - x[1];
  del2 = ( f[2] - f[1] ) / h2;
/*
  Set D(1) via non-centered three point formula, adjusted to be
  shape preserving.
*/
  hsum = h1 + h2;
  w1 = ( h1 + hsum ) / hsum;
  w2 = -h1 / hsum;
  d[0] = w1 * del1 + w2 * del2;

  if ( pchst ( d[0], del1 ) <= 0.0 )
  {
    d[0] = 0.0;
  }
/*
  Need do this check only if monotonicity switches.
*/
  else if ( pchst ( del1, del2 ) < 0.0 )
  {
     dmax = 3.0 * del1;

     if ( fabs ( dmax ) < fabs ( d[0] ) )
     {
       d[0] = dmax;
     }

  }
/*
  Loop through interior points.
*/
  for ( i = 2; i <= nless1; i++ )
  {
    if ( 2 < i )
    {
      h1 = h2;
      h2 = x[i] - x[i-1];
      hsum = h1 + h2;
      del1 = del2;
      del2 = ( f[i] - f[i-1] ) / h2;
    }
/*
  Set D(I)=0 unless data are strictly monotonic.
*/
    d[i-1] = 0.0;

    temp = pchst ( del1, del2 );

    if ( temp < 0.0 )
    {
      ierr = ierr + 1;
      dsave = del2;
    }
/*
  Count number of changes in direction of monotonicity.
*/
    else if ( temp == 0.0 )
    {
      if ( del2 != 0.0 )
      {
        if ( pchst ( dsave, del2 ) < 0.0 )
        {
          ierr = ierr + 1;
        }
        dsave = del2;
      }
    }
/*
  Use Brodlie modification of Butland formula.
*/
    else
    {
      hsumt3 = 3.0 * hsum;
      w1 = ( hsum + h1 ) / hsumt3;
      w2 = ( hsum + h2 ) / hsumt3;
      dmax = r8_max ( fabs ( del1 ), fabs ( del2 ) );
      dmin = r8_min ( fabs ( del1 ), fabs ( del2 ) );
      drat1 = del1 / dmax;
      drat2 = del2 / dmax;
      d[i-1] = dmin / ( w1 * drat1 + w2 * drat2 );
    }
  }
/*
  Set D(N) via non-centered three point formula, adjusted to be
  shape preserving.
*/
  w1 = -h2 / hsum;
  w2 = ( h2 + hsum ) / hsum;
  d[n-1] = w1 * del1 + w2 * del2;

  if ( pchst ( d[n-1], del2 ) <= 0.0 )
  {
    d[n-1] = 0.0;
  }
  else if ( pchst ( del1, del2 ) < 0.0 )
  {
/*
  Need do this check only if monotonicity switches.
*/
    dmax = 3.0 * del2;

    if ( fabs ( dmax ) < fabs ( d[n-1] ) )
    {
      d[n-1] = dmax;
    }

  }
  return 0;
}

int spline_pchip_val ( int n, local double x[], local double f[], local double d[],
  int ne, double xe[], double fe[] )
{
  int i;
  int ierc;
  int ierr;
  int ir;
  int j;
  int j_first;
  int j_new;
  int j_save;
  int next[2];
  int nj;
/*
  Check arguments.
*/
  if ( n < 2 )
  {
    ierr = -1;
    return ierr;
  }

  for ( i = 1; i < n; i++ )
  {
    if ( x[i] <= x[i-1] )
    {
      ierr = -3;
      return ierr;
    }
  }

  if ( ne < 1 )
  {
    ierr = -4;
    return ierr;
  }

  ierr = 0;
/*
  Loop over intervals.
  The interval index is IL = IR-1.
  The interval is X(IL) <= X < X(IR).
*/
  j_first = 1;
  ir = 2;

  for ( ; ; )
  {
/*
  Skip out of the loop if have processed all evaluation points.
*/
    if ( ne < j_first )
    {
      break;
    }
/*
  Locate all points in the interval.
*/
    j_save = ne + 1;

    for ( j = j_first; j <= ne; j++ )
    {
      if ( x[ir-1] <= xe[j-1] )
      {
        j_save = j;
        if ( ir == n )
        {
          j_save = ne + 1;
        }
        break;
      }
    }
/*
  Have located first point beyond interval.
*/
    j = j_save;

    nj = j - j_first;
/*
  Skip evaluation if no points in interval.
*/
    if ( nj != 0 )
    {
/*
  Evaluate cubic at XE(J_FIRST:J-1).
*/
      ierc = chfev ( x[ir-2], x[ir-1], f[ir-2], f[ir-1], d[ir-2], d[ir-1], 
        nj, xe+j_first-1, fe+j_first-1, next );

      if ( ierc < 0 )
      {
        ierr = -5;
        return ierr;
      }
/*
  In the current set of XE points, there are NEXT(2) to the right of X(IR).
*/
      if ( next[1] != 0 )
      {
        if ( ir < n )
        {
          ierr = -5;
          return ierr;
        }
/*
  These are actually extrapolation points.
*/
        ierr = ierr + next[1];

      }
/*
  In the current set of XE points, there are NEXT(1) to the left of X(IR-1).
*/
      if ( next[0] != 0 )
      {
/*
  These are actually extrapolation points.
*/
        if ( ir <= 2 )
        {
          ierr = ierr + next[0];
        }
        else
        {
          j_new = -1;

          for ( i = j_first; i <= j-1; i++ )
          {
            if ( xe[i-1] < x[ir-2] )
            {
              j_new = i;
              break;
            }
          }

          if ( j_new == -1 )
          {
            ierr = -5;
            return ierr;
          }
/*
  Reset J.  This will be the new J_FIRST.
*/
          j = j_new;
/*
  Now find out how far to back up in the X array.
*/
          for ( i = 1; i <= ir-1; i++ )
          {
            if ( xe[j-1] < x[i-1] )
            {
              break;
            }
          }
/*
  At this point, either XE(J) < X(1) or X(i-1) <= XE(J) < X(I) .

  Reset IR, recognizing that it will be incremented before cycling.
*/
          ir = i4_max ( 1, i-1 );
        }
      }

      j_first = j;
    }

    ir = ir + 1;

    if ( n < ir )
    {
      break;
    }

  }

  return 0;
}




/**
    * Function evaluates $\frac{\partial J}{\partial \theta} = x_1(1,\theta)^2 +x_2(1,\theta)^2$
    * where here 1 is the final time.  In other words x_1 and x_2 are the final values at t=1
    *
    * Note that here data is not used-it's a placeholder
    *
    * This is the imaginary version where $x_1^2$ has a real and imaginary component
    * In order to calculate the gradient we are interested in integrating the imaginary component only
    * in this simple linear system
    * Here $Im(\frac{\partial J}{\partial \theta})=2.x_1^R.x_1^I+2.x_2^R.x_2_I
    */
  	int imdJdtheta(double theta, local double ctl[], local double imctl[], local double tv[], local double d[], local double imd[], const int n, double tmin, double tmax,
  			       double* res) {
  		//We need to evaluate x1 and x2 for theta
  		//We need to integrate x1 and x2 using rfk45
  		/*int r8_rkf45 ( void f ( double t, double y[], double yp[] ), int neqn,
  		  double y[], double yp[], double *t, double tout, double *relerr,
  		  double abserr, int flag )*/
  		double x[4];
  		x[0]=-1.00;
  		x[1]=0.0;
  		x[2]=1.50+theta;
  		x[3]=0.0;
  		double xp[4];



  		  //This is the imaginary part of $\frac{\partial{J}}{\partial{\theta}}
  		  //printf("OpenCL x[0]=%e\n", x[0]);
  		  //printf("OpenCL x[1]=%e\n", x[1]);
  		  //printf("OpenCL x[2]=%e\n", x[2]);
  		  //printf("OpenCL x[3]=%e\n", x[3]);
  		  *res = 2*x[0]*x[1]+2*x[2]*x[3];

  		return 0;

  	}
  	/**
  	 * Performs Gauss Legendre integration for 16 points in one dimension-16 is the number of work items here hard coded
  	 */
  	int gaussLegInt(local double ctl[], local double imctl[], local double tv[], local double x[], local double w[], local double d[],
  			           local double imd[], const int n, double a, double b, double *psum) {
  		/* n = 32 */
 		 size_t groupId = get_group_id(0);

  		double A = 0.5*(b-a);
  		double B = 0.5*(b+a);
  		double s = 0.0;
  		int i=get_local_id(0);
  		double Ax=A*x[i];
  		//printf((__constant char *)"OpenCL for groupId=%d, localId=%d, Ax=%e\n", groupId, i, Ax);
  		double res1;
  		double res2;

  		int retCode1=imdJdtheta(B+Ax, ctl, imctl, tv, d, imd, n, 0, 1.0, &res1);
  		int retCode2=imdJdtheta(B-Ax, ctl, imctl, tv, d, imd, n, 0, 1.0, &res2);
  		s=w[i]*(res1+res2);
  		//printf((__constant char *)"OpenCL Jacobian element=%d, i=%d, s=%e, B+Ax=%e, B-Ax=%e, f(A+Bx)=%e, f(A-Bx)=%e, w[%d]=%e\n", groupId, i, s, B+Ax, B-Ax, res1, res2, i, w[i]);
  		if ( retCode1 == 0 && retCode2==0) {
  			*psum = A*s;
  			//printf("OpenCL psum=%e\n", *psum);
  			return 0;
  		}
  		else {
  			return 1; //Error code
  		}
  	
  	}


/**
 * @brief   Calculates the result of propagation of the dynamics for t=10 for different starting points
 * @param   control-the control data array
 * @param   sharedArray 
 * @param   binResult block-histogram array
 */
__kernel
void calcJacobian(	const __global double* u, //The control u
		            const __global double* imu,
		            const __global double* ts,
		       	   	__global double* jac) //Where the final result of the solution to the ODE is stored
{
#define  N 128  //TODO: Make this a compilation parameter
	local double ctl[N];
	local double imCtl[N];  //The imaginary part of the control is private to this thread
    local double tv[N];
	local double x[16];
	local double w[16];
	//These are the pchip coeffient
	local double d[N];
	local double imd[N];
	double thetaMin=-0.1;
	double thetaMax=0.1;

    size_t localId = get_local_id(0);
    size_t globalId = get_global_id(0);
    size_t groupId = get_group_id(0);
    size_t groupSize = get_local_size(0); //Number of local work items
	//Copy the global array to private memory
    //The loop is a one time hit
	double interval = 1.0/(N-1.0);
	//Set the Legendre variables-Note it does not matter if they are set more than once
	x[0]= 0.0483076656877383162348126;
	x[1]=0.1444719615827964934851864;
	x[2]=0.2392873622521370745446032;
	x[3]=0.3318686022821276497799168;
	x[4]=0.4213512761306353453641194;
	x[5]=0.5068999089322293900237475;
	x[6]=0.5877157572407623290407455;
	x[7]=0.6630442669302152009751152;
	x[8]=0.7321821187402896803874267;
	x[9]=0.7944837959679424069630973;
	x[10]=0.8493676137325699701336930;
	x[11]=0.8963211557660521239653072;
	x[12]=0.9349060759377396891709191;
	x[13]=0.9647622555875064307738119;
	x[14]=0.9856115115452683354001750;
	x[15]=0.9972638618494815635449811;

	w[0]=0.0965400885147278005667648;
	w[1]=0.0956387200792748594190820;
	w[2]=0.0938443990808045656391802;
	w[3]=0.0911738786957638847128686;
	w[4]=0.0876520930044038111427715;
	w[5]=0.0833119242269467552221991;
	w[6]=0.0781938957870703064717409;
	w[7]=0.0723457941088485062253994;
	w[8]=0.0658222227763618468376501;
	w[9]=0.0586840934785355471452836;
	w[10]=0.0509980592623761761961632;
	w[11]=0.0428358980222266806568786;
	w[12]=0.0342738629130214331026877;
	w[13]=0.0253920653092620594557526;
	w[14]=0.0162743947309056706051706;
	w[15]=0.0070186100094700966004071;
	//Need to copy global array to local array
	async_work_group_copy(ctl, u, N, 0);
	async_work_group_copy(imCtl, imu, N, 0);
	async_work_group_copy(tv, ts, N, 0);
	imCtl[groupId]=H;
	//Find the coefficient for the real portion of the control to be used downstream
	spline_pchip_set ( N, tv, ctl, d );
	spline_pchip_set (N, tv, imCtl, imd);
	double s;
	int retCode = gaussLegInt(ctl, imCtl, tv, x, w, d, imd, N, thetaMin, thetaMax, &s);

    local double scratch[16];
    scratch[localId]=s;
    barrier(CLK_LOCAL_MEM_FENCE);  //Make sure all the local elements have finished

    //We know we have 16 local elements
     //Here we need to add up all the previous work items and store the final in global memory 
    //Add them up in stages by adding alteranate elements at each stage to avoid race conditions
    for(int offset = get_local_size(0) / 2;
          offset > 0;
          offset >>= 1) {
        if (localId < offset) {
          double other = scratch[localId + offset];
          double mine = scratch[localId];
          scratch[localId] = mine+other;
        }
        barrier(CLK_LOCAL_MEM_FENCE);
      }


    jac[groupId]=scratch[0]/H;

#undef N
}





