/*
 * VanDerPolNLP.h
 *
 *  Created on: Jul 29, 2014
 *      Author: panos
 */

#ifndef VANDERPOLMIN_H_
#define VANDERPOLMIN_H_

#include <memory>
#include <cassert>
#include <glog/logging.h>
#include <thread>
#include "IpTNLP.hpp"


using namespace Ipopt;

namespace hyper {

/** Van Der Pol Oscillator solution using IPOPT.
 * This class solves the ODE explicitly before optimizing
 * and uses quadrature for integration
 *
 *
 */

class VanDerPolODE_NLP : public TNLP
{
protected:
	std::unique_ptr<VanDerPolGPU> vdp;
	size_t N, Nc, Nt;
	double Tf;
	size_t NGroups;
	size_t NThreads;
	double muMin;
	double muMax;
	double h;
	const  size_t NMu;
	const  size_t NNormPts; //Number of normalized points for interval [0,1]
	std::vector<double> xinit;

public:
	  std::vector<std::vector<double>> x1m ,x2m;

  /** default constructor
	  assert(init_z == false);
   *  Adds the GPU for ODE solution
   *  @param[in] N-the size of the control
   *  @param[in] Nc-the number of points used to interpolate the control
   *  */
  VanDerPolODE_NLP(size_t N, size_t Nc) : NMu(1024) , NNormPts(100) {
	  	  	double avMu = 0.5;
	  	  	//See http://www.cs.cornell.edu/~bindel/class/cs3220-s11/notes/lec24.pdf
	  	  	double T = (3.0-2*log(2))*avMu+4.676*0.5*pow(avMu, -1/3); //The period
	  	  	LOG(INFO) << "Period T=" << T << std::endl;
			this->Tf=5.0;
	  	  	LOG(INFO) << "Tf=" << Tf << std::endl;
	  	  	Nt = NNormPts * Tf;
			NGroups = 32;
			NThreads = 32;
			if ( NGroups*NThreads != NMu )
				throw std::runtime_error("VanDerPolODE_NLP: Number of threads and groups inconsistent with the number of discrete values of mu");
			muMin = 0.0;
			muMax = 1.0;
			h=sqrt(std::numeric_limits<double>::epsilon());
			LOG(INFO) << "Complex Derivative h=" << h << std::endl;
			this->N=N;
			std::vector<double> u(N, 0.0);
			std::string Options("-DNT=");
			Options += std::to_string(Nt);
			Options.append(" -DNC=");
			Options.append(std::to_string(Nc));
			std::unique_ptr<VanDerPolGPU> tptr( new VanDerPolGPU(1e-6, Options));
			cout << "Options=" << Options << std::endl;
			vdp= std::move(tptr);
			xinit.resize(4);
			xinit[0]=3.0;
			xinit[1]=0.0;
			xinit[2]=0.0;
			xinit[3]=0.0;
			this->Nc = Nc; //Number of interpolated control points
  }

  void printHello () {
		cout << "Hello" << std::endl;
  }

  /** default destructor */
  virtual ~VanDerPolODE_NLP() {

  }

  /**@name Overloaded from TNLP */
  //@{
  /** Method to return some info about the nlp
   *  Here the problem is one of no constraints
   */
  virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                            Index& nnz_h_lag, IndexStyleEnum& index_style) {
	  n=N;
	  m=2;
	  nnz_jac_g=2*N;
	  nnz_h_lag=0;
	  index_style = TNLP::C_STYLE;
	  return true;
  }

  /** Method to return the bounds for my problem */
  virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
                               Index m, Number* g_l, Number* g_u) {
	  for ( int i=0; i < n; i++) {
		  x_l[i]=-100.0;
		  x_u[i]=100.0;
	  }

	  g_l[0]=0.0;
	  g_u[0]=0.0;
	  g_l[1]=0.0;
	  g_u[1]=0.0;

	  return true;
  }

  /** Method to return the starting point for the algorithm */
  virtual bool get_starting_point(Index n, bool init_x, Number* x,
                                  bool init_z, Number* z_L, Number* z_U,
                                  Index m, bool init_lambda,
                                  Number* lambda) {
	  assert(init_x == true);
	  assert(init_z == false);
	  assert(init_lambda == false);
	  //First guess for the control
	  for ( int i=0; i< n; i++ ) {
         x[i]=-10.0 + 30.0*i/(n-1);
	  }
	  return true;
  }



  /** Method to return the objective value */
  virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value) {
          if ( n != N )
        	  throw std::runtime_error("Dimension of problem not set properly in Ipopt");
          if ( n == 0 )
        	  throw std::runtime_error("Dimension of control cannot be zero");
	  	  //Here we use quadrature on the GPU
	  	  //calcObjRK4GPU(std::vector<double>& ru, std::vector<double>& imu,  size_t Nt, size_t NGroups,
	      //              size_t NThreads, double Tf, double muMin, double muMax, double &reJ, double &imJ)
	  	  //We need to artificially separate the control
	  	  std::vector<double> ru(x, x+n); //copies the array into the vector
	  	  std::vector<double> imu(n, 0.0);
	  	  double reJ;
	  	  double imJ;

	  	  bool getAllPts = false;
	  	  double g1R, g1I, g2R, g2I;
	  	  std::vector<double> x1fR, x1fI, x2fR, x2fI;
	  	  /*
	  	   * calcObjRK4GPU( std::vector<double>& ru, std::vector<double>& imu,  size_t Nt, size_t Nc, size_t NGroups,
			           size_t NThreads, double Tf, double muMin, double muMax, std::vector<double>& xinit,
			           double &reJ, double& imJ, double& g1R, double& g1I, double& g2R, double& g2I)
	  	   */
	  	  vdp->calcObjRK4GPU(ru, imu, Nt, Nc, NGroups, NThreads, Tf, muMin, muMax, xinit, reJ, imJ, g1R, g1I, g2R, g2I);

	  	  obj_value = reJ;
	  	  //cout << "parallel obj_value=" << obj_value << std::endl;
	  	  //Compare with serial version
	  	//double refCalcObjSerial(std::vector<double> ctrl, double muMin, double muMax, size_t quadSize, size_t Nt,  double x10, double x20)
	  	  //double refObj = vdp->refCalcObjSerial(ru, 0.0, 1.0, NMu, 100, 0.0, 3.0, Tf);
	  	  //cout << "refObj=" << refObj << std::endl;
          //LOG(INFO) << "Difference between parallel and serial objective=" << abs(reJ-refObj);
          /*if ( abs(reJ-refObj) > 1e-8) {
        	  std::this_thread::sleep_for (std::chrono::seconds(2));
        	  throw std::runtime_error("Parallel and serial solutions do not agree");
          }*/
	  	  return true;

  }

  /** Method to return the gradient of the objective */
  virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f) {
	  std::vector<double> ru(x, x+n); //copies the array into the vector
      std::vector<double> imu(n, 0.0);
      double reJ;
	  double imJ;
	  bool getAllPts = false;
	  for ( int i=0; i < n; i++ ) {
		  imu[i]=h;
		  std::vector<std::vector<double>> x1m ,x2m;
		  double g1R, g1I, g2R, g2I;
	  	  std::vector<double> x1fR, x1fI, x2fR, x2fI;

		  //vdp->calcObjRK4GPU(ru, imu, Nt, NGroups, NThreads, Tf, muMin, muMax, reJ, imJ, x1m, x2m, getAllPts, g1R, g1I, g2R, g2I, false, false);
	  	  vdp->calcObjRK4GPU(ru, imu, Nt, Nc, NGroups, NThreads, Tf, muMin, muMax, xinit, reJ, imJ, g1R, g1I, g2R, g2I);

		  grad_f[i] = imJ/h; //Use the complex differentiation trick
		  imu[i]=0.0;
	  }
	  return true;
  }

  /** Method to return the constraint residuals */
  virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g) {
	  std::vector<double> ru(x, x+n); //copies the array into the vector
	  std::vector<double> imu(n, 0.0);
	  double reJ;
	  double imJ;
	  double g1R, g1I, g2R, g2I;
  	  std::vector<double> x1fR, x1fI, x2fR, x2fI;
  	  vdp->calcObjRK4GPU(ru, imu, Nt, Nc, NGroups, NThreads, Tf, muMin, muMax, xinit, reJ, imJ, g1R, g1I, g2R, g2I);

	  LOG(INFO) << "g1R=" << g1R << std::endl;

	  LOG(INFO) << "g2R=" << g2R << std::endl;
	  g[0]=g1R;
	  g[1]=g2R;
	  return true;
  }

  /** Method to return:
   *   1) The structure of the jacobian (if "values" is NULL)
   *   2) The values of the jacobian (if "values" is not NULL)
   */
  virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
                          Index m, Index nele_jac, Index* iRow, Index *jCol,
                          Number* values) {
	  assert(n==N);
	  if ( NULL == values ) {
		  cout << "n=" << n << std::endl;
		  for ( int i=0; i < n; i++) {
			  iRow[i]=0; jCol[i]=i;
			  iRow[i+n]=1; jCol[i+n]=i;
		  }

	  }
	  else {
		  std::vector<double> ru(x, x+n); //copies the array into the vector
		  std::vector<double> imu(n, 0.0);
		  double reJ;
		  double imJ;
		  bool getAllPts = false;
		  for ( int i=0; i < n; i++ ) {
			  imu[i]=h;
			  std::vector<std::vector<double>> x1m ,x2m;
			  double g1R, g1I, g2R, g2I;
			  std::vector<double> x1fR, x1fI, x2fR, x2fI;
		  	  vdp->calcObjRK4GPU(ru, imu, Nt, Nc, NGroups, NThreads, Tf, muMin, muMax, xinit, reJ, imJ, g1R, g1I, g2R, g2I);
		  	  //cout << "g1I=" << g1I << std::endl;
		  	  //cout << "g2I=" << g2I << std::endl;
		  	  values[i] = g1I/h; //Use the complex differentiation trick
			  values[i+n]=g2I/h;
			  imu[i]=0.0;
		  }
	  }
	  return true;
  }

  /** Method to return:
   *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
   *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
   */
  virtual bool eval_h(Index n, const Number* x, bool new_x,
                      Number obj_factor, Index m, const Number* lambda,
                      bool new_lambda, Index nele_hess, Index* iRow,
                      Index* jCol, Number* values) {
	  return false;
  }



  //@}

  /** @name Solution Methods */
  //@{
  /** This method is called when the algorithm is complete so the TNLP can store/write the solution */
  virtual void finalize_solution(SolverReturn status,
                                 Index n, const Number* x, const Number* z_L, const Number* z_U,
                                 Index m, const Number* g, const Number* lambda,
                                 Number obj_value, const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq) {
	  /*std::vector<double> ru(n);
	  for ( int i=0; i < n; i++ ) {
		  ru[i]=x[i];
	  }
	  std::vector<double> imu(n, 0.0);
	  double reJ;
	  double imJ;
	  std::vector<std::vector<double>> x1m ,x2m;
	  bool getAllPts = false;
	  LOG(INFO) << "Nt=" << Nt << std::endl;
	  double g1R, g1I, g2R, g2I;
	  vdp->calcObjRK4GPU(ru, imu, Nt, NGroups, NThreads, Tf, muMin, muMax, reJ, imJ, x1m, x2m, getAllPts, g1R, g1I, g2R, g2I, false, false);
	  size_t muDim = NGroups*NThreads;
	  double muInt = (muMax-muMin)/(muDim-1);
	  int numParalelMonterCarloRuns = 100000; // This is number of Monte Carlo runs x number of quadrature points
	  double g1RSum=0.0;
	  double g2RSum=0.0;
	  for ( size_t i=0; i < numParalelMonterCarloRuns; i++ ) {
		  vdp->calcObjRK4GPU(ru, imu, Nt, NGroups, NThreads, Tf, muMin, muMax, reJ, imJ, x1m, x2m, false, g1R, g1I, g2R, g2I, false, true);
		  g1RSum+=g1R;
		  g2RSum+=g2R;
	  }
	  LOG(INFO) << "Monte Carlo average g1R=" << g1RSum/numParalelMonterCarloRuns;
	  LOG(INFO) << "Monte Carlo average g2R=" << g2RSum/numParalelMonterCarloRuns;

	    //c1->Print("/tmp/plots.pdf");
	    //std::cout << "Creating pdf" << std::endl;
	  LOG(INFO) << "Optimal Control for Tf=" << Tf << std::endl;
	  LOG(INFO) << "N=" << n << std::endl;*/
}


  //@}

private:
  /**@name Methods to block default compiler methods.
   * The compiler automatically generates the following three methods.
   *  Since the default compiler implementation is generally not what
   *  you want (for all but the most simple classes), we usually
   *  put the declarations of these methods in the private section
   *  and never implement them. This prevents the compiler from
   *  implementing an incorrect "default" behavior without us
   *  knowing. (See Scott Meyers book, "Effective C++")
   *
   */
  //@{
  //  HS071_NLP();
  VanDerPolODE_NLP(const VanDerPolODE_NLP&);
  VanDerPolODE_NLP& operator=(const VanDerPolODE_NLP&);
  //@}
};

}; //namespace hyper




#endif /* VANDERPOLMIN_H_ */
