#ifndef _DFRIDR_H_
#define _DFRIDR_H_
#include "nr3.h"
/**
 * Returns the derivative of a function func at a point x by Ridders’ method of polynomial extrapolation.
 * The value h is input as an estimated initial stepsize; it need not be small, but rather should be an
 * increment in x over which func changes substantially. An estimate of the error in the derivative is returned as err.
 */
inline double dfridr(double (*func)(const double x), const double x, const double h, double &err)
{
	const int ntab=10;
	const double con=1.4, con2=(con*con);
	const double big=numeric_limits<double>::max();
	const double safe=2.0;
	int i,j;
	double errt,fac,hh,ans;
	MatDoub a(ntab,ntab);
	if (h == 0.0) throw("h must be nonzero in dfridr.");
	hh=h;
	a[0][0]=(func(x+hh)-func(x-hh))/(2.0*hh);
	err=big;
	for (i=1;i<ntab;i++) {
		hh /= con;
		a[0][i]=(func(x+hh)-func(x-hh))/(2.0*hh);
		fac=con2;
		for (j=1;j<=i;j++) {
			a[j][i]=(a[j-1][i]*fac-a[j-1][i-1])/(fac-1.0);
			fac=con2*fac;
			errt=MAX(abs(a[j][i]-a[j-1][i]),abs(a[j][i]-a[j-1][i-1]));
			if (errt <= err) {
				err=errt;
				ans=a[j][i];
			}
		}
		if (abs(a[i][i]-a[i-1][i-1]) >= safe*err) break;
	}
	return ans;
}

#endif
