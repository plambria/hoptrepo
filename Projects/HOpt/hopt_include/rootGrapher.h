/*
 * rootGrapher.h
 *
 *  Created on: Aug 10, 2014
 *      Author: panos
 */

#ifndef ROOTGRAPHER_H_
#define ROOTGRAPHER_H_

#include <thread>
#include "DynOptGPU.h"
#include "gauss_legendre.h"
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TList.h"


class RootGrapher {
private:
	std::vector<std::vector<double>> xdata;
	std::vector<std::vector<double>> ydata;
	std::string filename;
	std::string title;
	TCanvas* canvas;
	TGraph* gr;
	TMultiGraph *mg;

public:
	RootGrapher() {

	}

	void drawAsyncGraph(std::vector<std::vector<double>>& xd, std::vector<std::vector<double>>& yd, std::string filename, std::string title) {
		if ( xd.size() != yd.size() )
			throw std::runtime_error("drawAsyncGraph: Graphing Dimensions must match");
		xdata.resize(xd.size());
		ydata.resize(yd.size());
		for ( int i=0; i < yd.size(); i++ ) {
			std::vector<double> xdel = xd[i];
			xdata.push_back(xdel);
			std::vector<double> ydel = yd[i];
			ydata.push_back(ydel);
		}
		this->filename = filename;
		this->title = title;

		std::thread drawThread([this]() {
			TApplication::CreateApplication();
			TList* apps = TApplication::GetApplications();
			TApplication* app = (TApplication *) apps->First();
			//Give the canvas a random name
			canvas = new TCanvas("C1","Canvas",2000,2000);
		    canvas->SetGrid();
		    canvas->cd();
		    mg = new TMultiGraph();
		    std::vector<TGraph*> graphs;
		    for ( int i=0; i < xdata.size(); i++) {
		    	std::vector<double> x = xdata[i];
		    	std::vector<double> y = ydata[i];
			    gr = new TGraph(x.size(), &x[0], &y[0]);
			    graphs.push_back(gr);
			    mg->Add(gr);

		    }

		    mg->SetTitle(this->title.c_str());
		    mg->Draw("AP");

			canvas->Update();
			canvas->SaveAs(this->filename.c_str());
		    for ( int i=0; i< graphs.size(); i++ ) {
		    	delete graphs[i];
		    }
		    delete mg;
			delete canvas;


		});

		drawThread.detach();


	}
};




#endif /* ROOTGRAPHER_H_ */
