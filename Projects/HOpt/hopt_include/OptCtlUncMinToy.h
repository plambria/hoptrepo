/*
 * OptimCtlUncToy.h
 *
 *  Created on: Apr 17, 2014
 *      Author: panos
 */

#ifndef OPTIMCTLUNCTOY_H_
#define OPTIMCTLUNCTOY_H_

#include <IpTNLP.hpp>
#include <limits>
#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>

#include "dfridr.h"
#include "Polylib.h"
#include <glog/logging.h>
#include <zmq.hpp>
#include "nr3.h"
#include "zhelpers.hpp"
#include "MultiGraphPts.pb.h"
#include "dcomplex.h"
#include "FirstOrderGPUSolv.h"


#define tag_f 1
#define tag_g 2
#define tag_L 3
#define HPOFF 30

using namespace Ipopt;


void spline_pchip_set ( int n, double x[], double f[], double d[] );
void spline_pchip_val ( int n, double x[], double f[], double d[], int ne,
  double xe[], double fe[] );
double r8_epsilon();
int r8_rkf45 ( void f ( double t, double y[], double yp[] ), int neqn,
  double y[], double yp[], double *t, double tout, double *relerr,
  double abserr, int flag );


extern "C" {

  double gauss_legendre(int n, double (*f)(double,void*), void* data, double a, double b);
  int rkqs(float y[], float dydx[], int n, float *x, float htry, float eps,
  	float yscal[], float *hdid, float *hnext,
  	void (*derivs)(float, float [], float []));

  int odeint(float ystart[], int nvar, float x1, float x2, float eps, float h1,
  	float hmin, int *nok, int *nbad,
  	void (*derivs)(float, float [], float []),
  	int (*rkqs)(float [], float [], int, float *, float, float, float [],
  	float *, float *, void (*)(float, float [], float [])));
  float *vector(long nl, long nh);
  void free_vector(float *v, long nl, long nh);

}



namespace hyper {


/**************************************************************************************************
* Toy problem for uncertainty minimization
 *
If the problem is unconstrained, there are no constraints (m=0) and
no nonzeros in the Jacobian (nnz_jac_g=0).  However, the objective
function might still have non-zero entries in the Hessian, so nnz_h_lag
depends on your objective function (please check the documentation for the
definition of the Lagrangian Hessian).  I would return true for eval_g
eval_grad_g since they can correctly compute the values of the (zero)
constaints, but it probably doesn't matter.
 ****************************************************************************************************/

class OptCtlUncMinToy : public TNLP
{
private:
	int m, jacNonZeroes, hessNonZeros;
	Number x20, x10, pi, t0;
	static int uDim;
    static bool  objeval;
    static std::vector<double> optu;
    static std::vector<double> optt;
    //The following three vectors are for phase plotting
    static std::vector<double> x1;
    static std::vector<double> x2;
    static std::vector<double> tph;
	// Prepare our context and publisher
    zmq::context_t* context;
    zmq::socket_t* publisher;





public:
    static int N;
	static std::vector<double> ctl, tv, d, imCtl, imd; //d stores the coefficients used in SPLINE_PCHIP_VAL
    static double tf, tint, fObj; //Final time
    static int currGradIndex;
    static FirstOrderGPUSolv gpuSolv;

	OptCtlUncMinToy() {
		init();
	};

	  /** default constructor */
		OptCtlUncMinToy(int N) {
		   this->N=N;
           init();

	  };

	 void init() {
	      x10=-1.0;
	      x20=1.5;
		  m=0;  //this is an unconstrained problem
		  jacNonZeroes=0;
		  hessNonZeros=0;

		  pi=atan(1)*4.0;
		  t0=0.0;
		  OptCtlUncMinToy::ctl.clear();
		  imCtl.assign(N, 0.0);
		  this->tf=1.0;
          ctl.resize(N);
          //Very first guess for first time interval
          double dt=tf/(N-1);
          optu.resize(N);
          optt.resize(N);
          d.resize(N);
          for ( int i=0; i < N; i++) {
        	  optt[i]=i*dt;
        	  optu[i]=-1.8-2.3*optt[i];

          }
          spline_pchip_set ( N, &optt[0], &optu[0], &d[0] );
          tv.resize(N);
          d.resize(N);
          imd.resize(N);
          x1.resize(N);
          x2.resize(N);

          double xmin, xmax, ymin, ymax;
          xmin = -1.0;
          xmax = 0.5;
          ymin = 0.0;
          ymax = 2.0;
          context = new zmq::context_t(1);
          publisher = new zmq::socket_t(*context, ZMQ_PUB);

          publisher->bind("tcp://*:5556");

 	 }



	  /** default destructor */
	  virtual ~OptCtlUncMinToy() {
          //This has to be done in the following order
		  publisher->close();
		  delete publisher;
		  delete context;
	  };

	  /**
	   * Function evaluates the dynamics at point t given x-here x is the control u
	   */
	static void dynF(double t, double x[], double xp[]) {
		//We need to find u based on t-use pchip interpolation
		double iu[1];
		double tt[1];
		tt[0]=t;
		spline_pchip_val ( ctl.size(), &tv[0], &ctl[0], &d[0], 1,
		     tt, iu );
		//Now we have u
		double u=iu[0];
		//DLOG(INFO) << "PCHIP derived u=" << u << " at time t=" << t;
		xp[0]=x[1];
		xp[1]=pow(x[1], 1) + u;
	}


	  /**
	   * Function evaluates the dynamics at point t given x-here x is the control u
	   */
	static void dynFComplex(double t, double x[], double xp[]) {
		//We need to find u based on t-use pchip interpolation
		double pu[1];
		double tt[1];
		tt[0]=t;
		spline_pchip_val ( ctl.size(), &tv[0], &ctl[0], &d[0], 1, tt, pu );
		//Now we have the real part of u
		double u=pu[0];
        //Let find the imaginary part
		double iu[1];
		spline_pchip_val ( imCtl.size(), &tv[0], &imCtl[0], &imd[0], 1,
				     tt, iu );
		double imU=iu[0];
		xp[0]=x[2];
		xp[1]=x[3];
		xp[2]=pow(x[2], 1) + u;
		xp[3]=x[3]+imU;
	}



	static double calcImObjForGrad(double h ) {
		//At this point both ctl and imCtl have been set with uperturbed values earlier
        imCtl[currGradIndex]=h;

		//Set the interpolation parameters-overkill in this case
		spline_pchip_set ( imCtl.size(), &tv[0], &imCtl[0], &imd[0] );
#ifndef NDEBUG
		if ( currGradIndex == 0) {
			for ( int i=0; i < imd.size(); i++) {
				printf("CPU imd[%d]=%e\n", i, imd[i]);
			}
		}
#endif
		//Here we have the imaginary part and the real part of the control at position currGradIndex
		//and we can evaluate the Jacobian there.  To do so we need to determine the imaginary portion of the
		//objective given a small perturbation h in the imaginary direction
        double imObjValue = gauss_legendre(32, imdJdtheta, NULL, -0.1, 0.1);
		imCtl[currGradIndex]=0.0;
	   return imObjValue;
	}






	static double calcObjForGrad(const double x ) {
        double oldCtlai=ctl[currGradIndex]; //Remember so we can set it back
		ctl[currGradIndex]=x;
		//DLOG(INFO) << "Changing value of control for position " << currGradIndex << " to " << x;
        double obj_value = calcObj(ctl.size(), &ctl[0]);
		ctl[currGradIndex]= oldCtlai; //Set the value back for the next call
	   return obj_value;
	}




	static std::vector<double> calcObjWrapper(const std::vector<double>& x) {
		std::vector<double> res;
		res.reserve(x.size());
		int n = x.size();
		double obj = calcObj(n, &x[0]);
		res.push_back(obj);
		return res;
	}

	static void setCtlCalcPCHIP(int n, const double *u) {
		//DLOG(INFO) << "Setting the control array to:";
		ctl.resize(n);
		for ( int i=0; i < n; i++ ) {
			ctl[i]=u[i];
			//DLOG(INFO) << "u[" << i << "]=" << ctl[i];
		}
		d.resize(n);
        //Calculate the pchip coefficients
	    spline_pchip_set ( n, &tv[0], &ctl[0], &d[0] );//This is needed for correct evaluation of the objective
	}









  /**@name Overloaded from TNLP */
  //@{
  /** Method to return some info about the nlp */
  virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                            Index& nnz_h_lag, IndexStyleEnum& index_style) {
	  n=N;
	  m=0;
	  nnz_jac_g=0;
	  nnz_h_lag=0;
	  index_style = TNLP::C_STYLE;
	  return true;
  }

  /** Method to return the bounds for my problem */
  virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
                               Index m, Number* g_l, Number* g_u) {
	  for ( int i=0; i < n; i++) {
		  x_l[i]=-50.;
		  x_u[i]=50.;
	  }
	  return true;
  }

  void calcTimeSeries(int n) {
	  tv.resize(n);
	  //The new time variables will have different values since tf is different
      double dt = tf/(N-1);
		 for ( int i=0; i < n; i++) {
			 double tai=dt*i;
			 tv[i]=tai;
		 }
  }

  /** Method to return the starting point for the algorithm */
  virtual bool get_starting_point(Index n, bool init_x, Number* x,
                                  bool init_z, Number* z_L, Number* z_U,
                                  Index m, bool init_lambda,
                                  Number* lambda) {


	  tv.resize(n);

		  //The new time variables will have different values since tf is different
      calcTimeSeries(n);

		  //Here we have the control values from the previous iteration
			 //Use pchip interpolation to "guess" the new control values
		  spline_pchip_val(optt.size(), &optt[0], &optu[0], &d[0], n,  &tv[0], x);

	  return true;
  }



  /** Method to return the objective value */
  virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value) {
	  //LOG(INFO) << "eval_f called";
	  DLOG(INFO) << "Dimension of problem =" << n << std::endl;
	  objeval=true;
	  setCtlCalcPCHIP(n, x);
	  //Here f = (1/2)* integral from -0.1 to 0.1 of (x_1(1,theta)^2 + x_2(1,theta)^2) d theta
     obj_value=OptCtlUncMinToy::calcObj(n, x);
     //LOG(INFO) << "Calculated objective is" << obj_value;
     plotCurrCtl();
     plotPhaseForCurrCtl();
     return true;
  }

  /** Method to return the gradient of the objective */
  virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f) {
	  //LOG(INFO) << "eval_grad_f called";
	  objeval=false;
	  //DLOG(INFO) << "Candidate u is:";
	  //Set the control and calculate the interpolation factors for the real part of the control
	  setCtlCalcPCHIP(n, x);


	  //Copy the passed in control into a vector to be used for determining the jacobian
      //Use the imaginary trick to calculate the Jacobian
//	 double h=1e-20;  //For now this is a constant-seems to work for most things
//	  for ( int i=0; i < n; i++ ) {
//    	  currGradIndex=i;
//    	  double imJ=calcImObjForGrad(h);
//    	  grad_f[i]=imJ/h;
//      }

	  //The following methods' names are misleading-they sent messages to plotting listeners
      //plotCurrCtl();
      //plotPhaseForCurrCtl();
	 //GPU Part
	  std::vector<double> jac(N);
	  int N=n;
	  gpuSolv.calcJac(N, ctl, jac);
//	  for ( int i=0; i < n; i++ ) {
//		  printf("DIfference between GPU and CPU for Jacobian for element %d=%e\n", i, abs(grad_f[i]-jac[i]));
//	  }
	  //Use the GPU result
	  std::copy(jac.begin(), jac.end(), grad_f);

      return true;
  }

  void evalExtrap() {
		 //Here also populate the independent variable array
  }

  /** Method to return the constraint residuals */
  virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g) {
	  //Problem is unconstrained
	  return true;
  }

  /** Method to return:
   *   1) The structure of the jacobian (if "values" is NULL)
   *   2) The values of the jacobian (if "values" is not NULL)
   */
  virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
                          Index m, Index nele_jac, Index* iRow, Index *jCol,
                          Number* values) {
	  //Jacobian is 0 here since problem is uncostrained
	  return true;
  }

  /** Method to return:
   *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
   *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
   */
  virtual bool eval_h(Index n, const Number* x, bool new_x,
                      Number obj_factor, Index m, const Number* lambda,
                      bool new_lambda, Index nele_hess, Index* iRow,
                      Index* jCol, Number* values) {
	  return false; //We will use BFGS
  }

  //@}

  /** @name Solution Methods */
  //@{
  /** This method is called when the algorithm is complete so the TNLP can store/write the solution */
  virtual void finalize_solution(SolverReturn status,
                                 Index n, const Number* x, const Number* z_L, const Number* z_U,
                                 Index m, const Number* g, const Number* lambda,
                                 Number obj_value,
				 const IpoptData* ip_data,
				 IpoptCalculatedQuantities* ip_cq) {

	  //LOG(INFO) << "Final result";


	  plotCurrCtl();
	  plotPhaseForCurrCtl();


  }

  void plotCurrCtl() {
	     std::ostringstream titlestr;
	     	  titlestr << "Control plot for N=" << N;
	     std::string filename("control.pdf");
	     std::string xaxis("time");
	     std::string yaxis("u");
	     plotGraph(tv, ctl, titlestr.str(), filename,xaxis, yaxis);

  }




  void plotPhaseForCurrCtl() {
	  //Finally plot the phase diagram
	  //To do this we need to redo the calculations N times
	  std::vector<std::vector<double> > x1v;
	  std::vector<std::vector<double> > x2v;
      int np=20;
      x1v.resize(np);
      x2v.resize(np);
	  for ( int i=0; i <np; i++ ) {
		  double th=-0.1;
		  double fr=0.2/(np*1.0-1.0);
		  th=th+i*fr;
		  dJdtheta(th, NULL);
		  //copy elements over to avoid memory overwrites
		  std::vector<double> tx1(x1);
		  std::vector<double> tx2(x2);
		  x1v[i]=tx1;
		  x2v[i]=tx2;

	  }
	  std::ostringstream titlestr;
	  titlestr << "Phase plot for N=" << N;
      std::string filename("Phase.pdf");
      std::string xaxis("x1");
      std::string yaxis("x2");
	  plotMultiGraph(x1v, x2v, titlestr.str(), filename, xaxis, yaxis);

  }
  //@}
  void plotGraph(std::vector<double> x, std::vector<double> y, std::string title, std::string filename, std::string xaxis, std::string yaxis) {
	  //Plot the phase diagram
		GraphPoints gpts;
		gpts.set_title(title.c_str());
		GraphPoints_xcoords* xc = new GraphPoints_xcoords();
		GraphPoints_ycoords* yc = new GraphPoints_ycoords();
		for ( int i=0; i< x.size(); i++ ) {
			xc->add_x(x[i]);
			yc->add_y(y[i]);
		}
		gpts.set_allocated_xcords(xc);
		gpts.set_allocated_ycords(yc);
		//Serialize the message
		std::string ptsStr;

		gpts.SerializeToString(&ptsStr);
		int strLen = ptsStr.length();
		zmq::message_t message(strLen);
		memcpy(message.data(), ptsStr.c_str(), strLen);
		s_sendmore(*publisher, "U");
		s_send(*publisher, ptsStr);
  }


  void plotMultiGraph(std::vector<std::vector<double> > xv, std::vector<std::vector<double> > yv, std::string title, std::string filename, std::string xaxis, std::string yaxis) {
      MultiGraphPoints mgps;
      mgps.set_title(title.c_str());
      MultiGraphPoints_xcoords* pxcs= mgps.add_xcords();
      MultiGraphPoints_ycoords* pycs= mgps.add_ycords();
      for ( int i=0; i < xv.size(); i++ ) {
          MultiGraphPoints_xcoords* pxcs= mgps.add_xcords();
          MultiGraphPoints_ycoords* pycs= mgps.add_ycords();
          std::vector<double> xa=xv[i];
          std::vector<double> ya=yv[i];
          for ( int j=0; j < xa.size(); j++ ) {
        	  pxcs->add_x(xa[j]);
        	  pycs->add_y(ya[j]);
          }
      }

		//Serialize the message
		std::string ptsStr;

		mgps.SerializeToString(&ptsStr);
		int strLen = ptsStr.length();
		zmq::message_t message(strLen);
		memcpy(message.data(), ptsStr.c_str(), strLen);
		s_sendmore(*publisher, "P");
		s_send(*publisher, ptsStr);

  }
  /**
      * Function evaluates $\frac{\partial J}{\partial \theta} = x_1(1,\theta)^2 +x_2(1,\theta)^2$
      * where here 1 is the final time.  In other words x_1 and x_2 are the final values at t=1
      *
      * Note that here data is not used-it's a placeholder
      *
      * This is the imaginary version where $x_1^2$ has a real and imaginary component
      * In order to calculate the gradient we are interested in integrating the imaginary component only
      * in this simple linear system
      * Here $Im(\frac{\partial J}{\partial \theta})=2.x_1^R.x_1^I+2.x_2^R.x_2_I
      */
    	static double imdJdtheta(double theta, void *data) {
    		//We need to evaluate x1 and x2 for theta
    		double t0=0;
    		//We need to integrate x1 and x2 using rfk45
    		/*int r8_rkf45 ( void f ( double t, double y[], double yp[] ), int neqn,
    		  double y[], double yp[], double *t, double tout, double *relerr,
    		  double abserr, int flag )*/
    		double x[4];
    		x[0]=-1.00;
    		x[1]=0.0;
    		x[2]=1.50+theta;
    		x[3]=0.0;
    		double xp[4];


    		//The parameters below are taken straight out of the provided examples
    		double abserr = sqrt ( r8_epsilon ( ) );
    		double relerr = sqrt ( r8_epsilon ( ) );
    		int flag = -1; //Single step mode.  Let the routine figure out h

    		double t_start = 0.0;
    		double t_stop = tf;

    		int n_step = 1; //Make the number of steps the same to correspond to the same values as tv vector


    		double t = 0.0;
    		double t_out = 0.0;
    	    int NEQN=4;
    	    //Initialize
    		dynFComplex ( t, &x[0],  &xp[0] );


         int actNumSteps=0;
    		  for ( unsigned i_step = 1; i_step <= n_step; i_step++ )
    		  {
    		    t = ( ( double ) ( n_step - i_step + 1 ) * t_start
    		        + ( double ) (          i_step - 1 ) * t_stop )
    		        / ( double ) ( n_step              );

    		    t_out = ( ( double ) ( n_step - i_step ) * t_start
    		            + ( double ) (	   i_step ) * t_stop )
    		            / ( double ) ( n_step );
    		    actNumSteps++;
             while ( flag < 0 || flag == 4) {
    		       flag = r8_rkf45 ( dynFComplex, NEQN, &x[0], &xp[0], &t, t_out, &relerr, abserr, flag );
    		       //Used for plotting
                   x1.push_back(x[0]);
                   x2.push_back(x[1]);


             }
             if (flag==2 || flag == 3) { //We've reached the desired point
             	flag=-2;  //Set the flag for next step
             }
             else if ( flag == 4 )
             	continue; //leave flag as 4 in this case and try again-see function documentation
             else if ( flag == 7 )
            	 flag = -2;
             else {
             	break;  //Here if we had an error it means we reached some kind of edge point for our problem
             }

    		  }
    		  if ( (t_out < t_stop) || flag > 4) {//This means we had some kind of error
    			  //LOG(INFO) << "Integration unsuccesful with flag=" << flag;
    			  x[0]=1e9; //Important to set these to a high figure to indicate finite escape velocity
    			  x[1]=1e9;
    		  }
    		  else {

    			  //LOG(INFO) << "Integration succesful with flag=" << flag;
    			  //Here print the control so we can use it for future warm startups
    			  if ( objeval ) {
    				  //DLOG(INFO) << "Control used:";
    				  for ( int k=0; k < uDim; k++) {
    					  //DLOG(INFO) << ctl[k];
    				  }



    			  }

    		  }

    		  //This is the imaginary part of $\frac{\partial{J}}{\partial{\theta}}
    	      double res = 2*x[0]*x[1]+2*x[2]*x[3];

    		return res;

    	}




  /**
     * Function evaluates $\frac{\partial J}{\partial \theta} = x_1(1,\theta)^2 +x_2(1,\theta)^2$
     * where here 1 is the final time.  In other words x_1 and x_2 are the final values at t=1
     *
     * Note that here data is not used-it's a placeholder
     */
   	static double dJdtheta(double theta, void *data) {
   //		double u=ctl[0];
   //		const Doub atol=1e-4, rtol=atol, h1=0.1, hmin=0.0, t1=0.0, t2=tf; VecDoub xstart(nvar);
   //		xstart[0]=-1.0;
   //		xstart[1]=1.5+theta;
   //		Output out(20); //Dense output at 20 points plus x1.
   //		rhs_van cb; //Declare d as a rhs_van object.
   //		Odeint<StepperDopr5<rhs_van> > ode(xstart,t1,t2,atol,rtol,h1,hmin,out,cb);
   //		try {
   //		   ode.integrate();
   //		}
   //		catch(...) {
   //           xstart[0]=1e10;
   //           xstart[1]=1e10;
   //		}
   		//We need to evaluate x1 and x2 for theta
   		double t0=0;
   		//We need to integrate x1 and x2 using rfk45
   		/*int r8_rkf45 ( void f ( double t, double y[], double yp[] ), int neqn,
   		  double y[], double yp[], double *t, double tout, double *relerr,
   		  double abserr, int flag )*/
   		if ( abs(theta) > 0.1 )
   			throw "Theta is out of range in function dJtheta";
   		std::vector<double> x;
   		x.reserve(2);
   		x.push_back(-1.00);
   		x.push_back(1.50+theta);
   		std::vector<double> xp;
   		xp.reserve(2);
   		xp.push_back(0);
   		xp.push_back(0);


   		//The parameters below are taken straight out of the provided examples
   		double abserr = sqrt ( r8_epsilon ( ) );
   		double relerr = sqrt ( r8_epsilon ( ) );
   		int flag = -1; //Single step mode.  Let the routine figure out h

   		double t_start = 0.0;
   		double t_stop = tf;

   		int n_step = 1; //Make the number of steps the same to correspond to the same values as tv vector


   		double t = 0.0;
   		double t_out = 0.0;
   	    int NEQN=2;
   	    x1.clear();
   	    x2.clear();
   	    tph.clear();

   	    x1.push_back(x[0]);
   	    x2.push_back(x[1]);
   	    //Initialize
   		dynF ( t, &x[0],  &xp[0] );


        int actNumSteps=0;
   		  for ( unsigned i_step = 1; i_step <= n_step; i_step++ )
   		  {
   		    t = ( ( double ) ( n_step - i_step + 1 ) * t_start
   		        + ( double ) (          i_step - 1 ) * t_stop )
   		        / ( double ) ( n_step              );

   		    t_out = ( ( double ) ( n_step - i_step ) * t_start
   		            + ( double ) (	   i_step ) * t_stop )
   		            / ( double ) ( n_step );
   		    actNumSteps++;
            while ( flag < 0 || flag == 4) {
   		       flag = r8_rkf45 ( dynF, NEQN, &x[0], &xp[0], &t, t_out, &relerr, abserr, flag );

                  x1.push_back(x[0]);
                  x2.push_back(x[1]);


            }
            if (flag==2 || flag == 3) { //We've reached the desired point
            	flag=-2;  //Set the flag for next step
            }
            else if ( flag == 4 )
            	continue; //leave flag as 4 in this case and try again-see function documentation
            else if ( flag == 7 )
           	 flag = -2;
            else {
            	break;  //Here if we had an error it means we reached some kind of edge point for our problem
            }

   		  }
   		  if ( (t_out < t_stop) || flag > 4) {//This means we had some kind of error
   			  //LOG(INFO) << "Integration unsuccesful with flag=" << flag;
   			  x[0]=1e9; //Important to set these to a high figure to indicate finite escape velocity
   			  x[1]=1e9;
   		  }
   		  else {

   			  //LOG(INFO) << "Integration succesful with flag=" << flag;
   			  //Here print the control so we can use it for future warm startups
   			  if ( objeval ) {
   				  //DLOG(INFO) << "Control used:";
   				  for ( int k=0; k < uDim; k++) {
   					  //DLOG(INFO) << ctl[k];
   				  }



   			  }

   		  }
   		  //Store these static variables for subsequent retrieval

   		  //DLOG(INFO) << "Integration result x1=" << x[0] << " x2=" << x[1];
          //Regardless proceed using the last successful result


   	      double res = pow(x[0],2) + pow(x[1], 2);

   		return res;

   	}

  static double calcObj(int n, const double *u ) {
  	setCtlCalcPCHIP(n, u); //Set the control array and interpolation coefficients
  	double obj_value;


      //LOG(INFO) << "Integrating across theta";
      obj_value=gauss_legendre(32,dJdtheta,NULL,-0.1,0.1);
     return obj_value;
  }


private:

    /**@name Methods to block default compiler methods.
   * The compiler automatically generates the following three methods.
   *  Since the default compiler implementation is generally not what
   *  you want (for all but the most simple classes), we usually
   *  put the declarations of these methods in the private section
   *  and never implement them. This prevents the compiler from
   *  implementing an incorrect "default" behavior without us
   *  knowing. (See Scott Meyers book, "Effective C++")
   *
   */
  //@{
  //  MyNLP();
   OptCtlUncMinToy(const OptCtlUncMinToy&);
   OptCtlUncMinToy& operator=(const OptCtlUncMinToy&);
  //@}

};


};  //namespace hyper










#endif /* OPTIMCTLUNCTOY_H_ */
