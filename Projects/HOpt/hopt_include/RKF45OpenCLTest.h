/**********************************************************************
 Copyright �2013 Advanced Micro Devices, Inc. All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 �   Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 �   Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************/

#ifndef RKF45_OPENCL_H_
#define RKF45_OPENCL_H_

#include "DynOptGPU.h"
#include "spline.h"
#include "rkf45.h"

double frunge ( double x );

void spline_pchip_set ( int n, double x[], double f[], double d[] );
void spline_pchip_val ( int n, double x[], double f[], double d[], int ne,
  double xe[], double fe[] );



int NN=128;
//UGLY:Globals needed for the test
//TODO: Put in own namespace
std::vector<double> tv(NN, 0.0);
std::vector<double> ctl(NN, 0.0);
std::vector<double> d(NN, 0.0);
std::vector<double> imCtl(NN, 0.0);
std::vector<double> imd(NN, 0.0);

/**
 * Function evaluates the dynamics at point t given x-here x is the control u
 */
void dynFComplex(double t, double x[], double xp[]) {

	//We need to find u based on t-use pchip interpolation
	double pu[1];
	double tt[1];
	tt[0]=t;
	spline_pchip_val ( ctl.size(), &tv[0], &ctl[0], &d[0], 1, tt, pu );
	//Now we have the real part of u
	double u=pu[0];
  //Let find the imaginary part
	double iu[1];
	spline_pchip_val ( imCtl.size(), &tv[0], &imCtl[0], &imd[0], 1,
			     tt, iu );
	double imU=iu[0];
	xp[0]=x[2];
	xp[1]=x[3];
	xp[2]=pow(x[2], 1) + u;
	xp[3]=x[3]+imU;

};

/**
 * RKF45OpenCLTest Test for RKF45 on the GPU
*/
class RKF45OpenCLTest: public hyper::DynOptGPU {
protected:
	std::string Options;

public:

	/**
	 * Constructor-All this inititialization is boileplate code
	 * Initialize member variables
	 * @param name name of sample (string)
	 */
	RKF45OpenCLTest() :
			DynOptGPU("RKF45_kernel.cl", Options) {


	}

	virtual ~RKF45OpenCLTest() {
	}



	void RKF45Test() {
		int N=128;
		double diff;
		int i;
		printf("\n");
		printf("GPU RKF45 Port Test\n");
		std::vector<double> u(N, 0.0);
		std::vector<double> imu(N, 0.0);
		std::vector<double> theta(1, 0.0);
		/*__kernel void testRFK45(const __global double* u,
				const __global double* imu,
				const __global double* theta,
				__global double* xout)*/
		cl_int err = CL_SUCCESS;
		// Create kernel object
		cl::Kernel kernel(program, "testRKF45", &err);
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to create kernel for testRKF45");

		// Create command queue for first device
		// Set command queue properties
		cl_command_queue_properties prop = 0;
		prop |= CL_QUEUE_PROFILING_ENABLE;
		cl::CommandQueue queue(*contextPtr, devices[0], prop, &err);
		if ( CL_SUCCESS != err )
			throw runtime_error("Unable to create queue");

		cl_int eventStatus = CL_QUEUED;

		// Set input data
		cl::Event inMapEvtCtl;
		cl::Event inUnmapEvtCtl;
		cl::Event outMapEvt;
		cl::Event outUnmapEvt;

		// Create device memory buffers
		// Set Persistent memory only for AMD platform
		cl_mem_flags inMemFlags = CL_MEM_READ_ONLY;
		inMemFlags |= CL_MEM_USE_PERSISTENT_MEM_AMD; //For AMD platform
		// Create buffer for control
		cl::Buffer uBuf, imuBuf, thetaBuf;
		enqWriteBuf(u, uBuf);
		enqWriteBuf(imu, imuBuf);
		enqWriteBuf(theta, thetaBuf);
		//Output buffers
		size_t bytesX=4*sizeof(cl_double);
		cl::Buffer xBuf = cl::Buffer(*contextPtr,
				CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, bytesX,
				NULL, &err);
		checkSuccess(err, "Unable to create buffer for X");



		// Bind kernel arguments to kernel
		err = kernel.setArg(0, uBuf);
		checkSuccess(err, "Unable to set u argument for kernel");
		err = kernel.setArg(1, imuBuf);
		checkSuccess(err, "Unable to set imu argument for kernel");
		kernel.setArg(2, thetaBuf);
		checkSuccess(err, "Unable to set thetaBuf argument for kernel");
		kernel.setArg(3, xBuf);
		checkSuccess(err, "Unable to set xBuf argument for kernel");

		runJob(kernel, 1, 1); //Run just one work item with one group
		// Read back the jacobian
		std::vector<double> x(4);
		readBuf(x, xBuf);
		printf("GPU Solutions versus exact\n");
		printf("Real part of x1=%e(computed) vs %e(exact)\n", x[0], 1.5*exp(1)-2.5);
		printf("Imaginary part of x1=%e(computed) vs %e(exact)\n", x[1], 0.0);
		printf("Real part of x2=%e(computed) vs %e(exact)\n", x[2], 1.5*exp(1.0));
		printf("Imaginary part of x2=%e(computed) vs %e(exact)\n", x[3], 0.0);
		//Compare to the standard c version
		testRKF45CVersion(0.0);
	}



	void testRKF45CVersion(double theta) {

		//Now run the standard C version
	const int  N=128;
	double x[4];
	x[0]=-1.00;
	x[1]=0.0;
	x[2]=1.50+theta;
	x[3]=0.0;
	double xp[4];
	for ( int i=0; i < N; i++ ) {
		ctl[i] = 0.0;
		imCtl[i] = 0.0;
		tv[i]=(1.0*i)/((double)(N-1));
	}


	//The parameters below are taken straight out of the provided examples
	double abserr = sqrt ( r8_epsilon ( ) );
	double relerr = sqrt ( r8_epsilon ( ) );

	printf("From C abserr=%e\n", abserr);
	printf("From C relerr=%e\n", relerr);

	int flag = -1;//Single step mode.  Let the routine figure out h

	double t_start = 0.0;
	double t_stop = 1.0;

	int n_step = 1;//Make the number of steps the same to correspond to the same values as tv vector

	double t = 0.0;
	double t_out = 0.0;
	spline_pchip_set ( N, &tv[0], &ctl[0], &d[0] );
	spline_pchip_set ( N, &tv[0], &imCtl[0], &imd[0] );
	//Initialize
	dynFComplex ( t, x, xp);

	int actNumSteps=0;

	for ( unsigned i_step = 1; i_step <= n_step; i_step++ )
	{
		t = ( ( double ) ( n_step - i_step + 1 ) * t_start
				+ ( double ) ( i_step - 1 ) * t_stop )
		/ ( double ) ( n_step );

		t_out = ( ( double ) ( n_step - i_step ) * t_start
				+ ( double ) ( i_step ) * t_stop )
		/ ( double ) ( n_step );
		actNumSteps++;
		while ( flag < 0 || flag == 4) {

			flag = r8_rkf45 ( dynFComplex, 4, x, xp, &t, t_out, &relerr, abserr, flag);

			//printf("After r8_rfk45 h=%e\n", h);

		}
		if (flag==2 || flag == 3) { //We've reached the desired point
			flag=-2;//Set the flag for next step
		}
		else if ( flag == 4 )
		continue; //leave flag as 4 in this case and try again-see function documentation
		else if ( flag == 7 )
		flag = -2;
		else {
			break; //Here if we had an error it means we reached some kind of edge point for our problem
		}

	}
	if ( (t_out < t_stop) || flag > 4) { //This means we had some kind of error
		printf("Error during invoking RFK45 return flag=%d\n", flag);
	}
	else {
		printf("Solution found-flag=%d, final time=%e\n", flag, t);

	}


	//This is the imaginary part of $\frac{\partial{J}}{\partial{\theta}}
	double xout[4];
	xout[0]=x[0];
	xout[1]=x[1];
	xout[2]=x[2];
	xout[3]=x[3];
	printf("Solutions from standard C version versus exact\n");
	printf("Real part of x1=%e(computed) vs %e(exact)\n", x[0], 1.5*exp(1)-2.5);
	printf("Imaginary part of x1=%e(computed) vs %e(exact)\n", x[1], 0.0);
	printf("Real part of x2=%e(computed) vs %e(exact)\n", x[2], 1.5*exp(1.0));
	printf("Imaginary part of x2=%e(computed) vs %e(exact)\n", x[3], 0.0);

	}

};
#endif
