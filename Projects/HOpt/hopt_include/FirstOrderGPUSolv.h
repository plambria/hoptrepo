/**********************************************************************
 Copyright �2013 Advanced Micro Devices, Inc. All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 �   Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 �   Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************/

#ifndef FIRST_ORDER_GPU_H_
#define FIRST_ORDER_GPU_H_

#include "DynOptGPU.h"
#include "spline.h"
#include "rkf45.h"

double frunge ( double x );

void spline_pchip_set ( int n, double x[], double f[], double d[] );
void spline_pchip_val ( int n, double x[], double f[], double d[], int ne,
  double xe[], double fe[] );

namespace hyper {

/**
 * DynOptGPU
 * Class implements an OpenCL based solution for the dynamical system
 * $\dot{x_1}=1\frac{1}{10} x_1-x_2-\frac{3}{4} x_1^2 -\frac{3}{4} x_1 x_2 -\frac{3}{2}x_2^2$
 * $\dot{x_2}=x_1$
 * subject to
 * $x_1(t=0)=U(0, 1/3) = \xi_1$
 * $x_2(t=0)=U(0, 1/3) = \xi_2$
 * $min J= \frac{1}{2} x_2^2(t=10) +\frac{1}{2} \int_0_10 (x_1^2+x_2^2+u^2)dt$
 */

class FirstOrderGPUSolv: public DynOptGPU {
private:
	std::string Options;

public:

	/**
	 * Constructor-All this inititialization is boileplate code
	 * Initialize member variables
	 * @param name name of sample (string)
	 */
	FirstOrderGPUSolv() : DynOptGPU("FirstOrderProb_kernels.cl", Options) {


	}

	virtual ~FirstOrderGPUSolv() {
	}


	void calcJac(int N, const std::vector<double>& u, std::vector<double>& jac) {
		cl_int err = CL_SUCCESS;
		// Create kernel object
		cl::Kernel kernel(program, "calcJacobian", &err);
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to create kernel for calcJacobian");

		// Create command queue for first device
		// Set command queue properties
		cl_command_queue_properties prop = 0;
		prop |= CL_QUEUE_PROFILING_ENABLE;
		cl::CommandQueue queue(*contextPtr, devices[0], prop, &err);
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to create queue");
		size_t bytes = N * sizeof(double);
		cl_int eventStatus = CL_QUEUED;

		// Set input data
		cl::Event inMapEvtCtl;
		cl::Event inUnmapEvtCtl;
		cl::Event outMapEvt;
		cl::Event outUnmapEvt;

		// Create device memory buffers
		// Set Persistent memory only for AMD platform
		cl_mem_flags inMemFlags = CL_MEM_READ_ONLY;
		inMemFlags |= CL_MEM_USE_PERSISTENT_MEM_AMD; //For AMD platform
		// Create buffer for control
		size_t bytesJac = u.size() * sizeof(cl_double);
		cl::Buffer ctlBuf;
		enqWriteBuf(u, ctlBuf);
		cl::Buffer jacBuf = cl::Buffer(*contextPtr,
				CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, bytesJac,
				NULL, &err);
		checkSuccess(err, "Unable to create buffer for jacobian");
		enqWriteBuf(u, ctlBuf);
		void* outMapPtr = NULL;

		// Bind kernel arguments to kernel
		err = kernel.setArg(0, ctlBuf);
		checkSuccess(err, "Unable to set ctl argument for kernel");
		kernel.setArg(1, jacBuf);
		checkSuccess(err, "Unable to set jacobian argument for kernel");
		runJob(kernel, 16, N);
		// Read back the jacobian
		jac.resize(N);
		readBuf(jac, jacBuf);

	}

	void testPCHIP() {
# define N 21
# define NE 101

		double diff;
		int i;
		printf("\n");
		printf("PCHIP Test\n");
		printf("  SPLINE_PCHIP_SET sets up a piecewise cubic\n");
		printf("    Hermite interpolant.\n");
		printf("  SPLINE_PCHIP_VAL evaluates the interpolant.\n");
		printf("\n");
		std::vector<double> x(N);
		std::vector<double> f(N);
		//
		//  Compute Runge's function at N points in [-1,1].
		//
				for (i = 0; i < N; i++) {
					x[i] = -1.0 + (double) (i) / 10.0;
					f[i] = frunge(x[i]);
				}
		//

		cl_int err = CL_SUCCESS;
		// Create kernel object
		cl::Kernel kernel(program, "testPCHIP", &err);
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to create kernel for calcJacobian");

		// Create command queue for first device
		// Set command queue properties
		cl_command_queue_properties prop = 0;
		prop |= CL_QUEUE_PROFILING_ENABLE;
		cl::CommandQueue queue(*contextPtr, devices[0], prop, &err);
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to create queue");
		size_t bytes = N * sizeof(double);
		cl_int eventStatus = CL_QUEUED;

		// Set input data
		cl::Event inMapEvtCtl;
		cl::Event inUnmapEvtCtl;
		cl::Event outMapEvt;
		cl::Event outUnmapEvt;

		// Create device memory buffers
		// Set Persistent memory only for AMD platform
		cl_mem_flags inMemFlags = CL_MEM_READ_ONLY;
		inMemFlags |= CL_MEM_USE_PERSISTENT_MEM_AMD; //For AMD platform
		// Create buffer for control
		size_t bytesOut = NE * sizeof(cl_double);
		cl::Buffer xBuf;
		cl::Buffer fxBuf;
		enqWriteBuf(x, xBuf);
		enqWriteBuf(f, fxBuf);
		//Output buffers
		size_t bytesXe= NE*sizeof(cl_double);
		size_t bytesFe=NE*sizeof(cl_double);
		cl::Buffer xeBuf = cl::Buffer(*contextPtr,
				CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, bytesXe,
				NULL, &err);
		checkSuccess(err, "Unable to create buffer for Xe");
		cl::Buffer feBuf = cl::Buffer(*contextPtr,
				CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, bytesFe,
				NULL, &err);
		checkSuccess(err, "Unable to create buffer for Fe");

		void* xeMapPtr = NULL;
		void* feMapPtr = NULL;

		// Bind kernel arguments to kernel
		err = kernel.setArg(0, xBuf);
		checkSuccess(err, "Unable to set x argument for kernel");
		err = kernel.setArg(1, fxBuf);
		checkSuccess(err, "Unable to set f argument for kernel");
		kernel.setArg(2, xeBuf);
		checkSuccess(err, "Unable to set xe argument for kernel");
		kernel.setArg(3, feBuf);
		checkSuccess(err, "Unable to set fe argument for kernel");

		runJob(kernel, 1, 1); //Run just one work item with one group
		// Read back the jacobian
		std::vector<double> xe(NE), fe(NE);
		readBuf(xe, xeBuf);
		readBuf(fe, feBuf);
		//Perform the CPU test as well
		std::vector<double> d(N);
		spline_pchip_set ( N, &x[0], &f[0], &d[0] );
		std::vector<double> xe_cpu(NE);
		  for ( i = 0; i < NE; i++ )
		  {
		    xe_cpu[i] = -1.0 + ( double ) ( i ) / ( double ) ( NE - 1 );
		  }
		std::vector<double> fe_cpu(NE);
		spline_pchip_val ( N, &x[0], &f[0], &d[0], NE, &xe_cpu[0], &fe_cpu[0] );
		//  Print the table of X, F(exact) and F(interpolated)
		//
		double diff_cpu, diffdiff;
				for (i = 0; i < NE; i++) {
					diff = fe[i] - frunge(xe[i]);
                    diff_cpu = fe_cpu[i] -frunge(xe_cpu[i]);
                    diffdiff = diff-diff_cpu;
					printf("  %8g  %10g  %10g  %10g %10g %10g\n", xe[i], frunge(xe[i]), fe[i],
							diff, diff_cpu, diffdiff);
				}

				return;
		# undef N
		# undef NE


	}


};

};  //namespace hyper
#endif
