/*
 * DataExporter.h
 *
 *  Created on: Aug 11, 2014
 *      Author: panos
 */

#ifndef DATAEXPORTER_H_
#define DATAEXPORTER_H_
#include <iostream>
#include <fstream>
#include <thread>

class DataExporter {
private:
	std::vector<double> xdata;
	std::vector<double> ydata;
	std::string filename;

public:
	void exportSimpleFormat(std::vector<double>& xd, std::vector<double>& yd, std::string filename) {
		if ( xd.size() != yd.size() )
			throw std::runtime_error("exportSimpleFormat: Vector Dimensions must match");
		if ( xd.size() == 0 || yd.size() == 0 )
			throw std::runtime_error("exportSimpleFormat: Vector Dimensions cannot be zero");
		xdata = xd;
		ydata = yd;
		this->filename = filename;

		std::thread storeThread([this]() {
			ofstream storeFile;
			storeFile.open(this->filename);
			for ( int i=0; i < xdata.size(); i++ ) {
				//std::setprecision(15)
				storeFile  << xdata[i] << " " << ydata[i] << std::endl;
			}
			storeFile.close();

		});

		storeThread.detach();


	}

};



#endif /* DATAEXPORTER_H_ */
