/*
 * dcomplex.h
 *
 *  Created on: May 26, 2014
 *      Author: panos
 */

#ifndef DCOMPLEX_H_
#define DCOMPLEX_H_
#include <math.h>
#include <stdbool.h>

typedef struct DCOMPLEX {
	double r;
    double i;
} dcomplex;

dcomplex Cadd(dcomplex a, dcomplex b);
dcomplex Csub(dcomplex a, dcomplex b);
dcomplex Cmul(dcomplex a, dcomplex b);


dcomplex Conjg(dcomplex z);

dcomplex Cdiv(dcomplex a, dcomplex b);

double Cabs(dcomplex z);
dcomplex Csqrt(dcomplex z);
dcomplex RCmul(double x, dcomplex a);
bool Ceq(dcomplex x, dcomplex y);

#endif /* DCOMPLEX_H_ */
