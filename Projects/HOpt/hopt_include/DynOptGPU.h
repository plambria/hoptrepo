#ifndef DYNOPTGPU_H_
#define DYNOPTGPU_H_

#include <cl.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <CLUtil.hpp>



int GetTimeMs();


namespace hyper {
/**
 * DynOptGPU
 * Class encapsulates the ugly OpenCL setup so the the submission of kernels is easier
 */

class DynOptGPU {
protected:
	cl_double setupTime; /**< time taken to setup OpenCL resources and building kernel */
	cl_double kernelTime; /**< time taken to run kernel and read result back */

	cl::Context* contextPtr; /**< CL context */
	std::vector<cl::Device> devices; /**< CL device list */

	cl::Program program; /**< CL program  */
	int iterations; /**< Number of iterations for kernel execution */
	bool loadBinaryFile;

	cl_bool byteRWSupport;
	std::string programName;
	cl::CommandQueue* queuePtr;
	std::string buildOptions;

public:
	std::string binFileName;

	/**
	 * Constructor-All this inititialization is boileplate code
	 * Initialize member variables
	 * @param name name of sample (string)
	 */
	DynOptGPU(std::string clFileName, const std::string& addOptions) :
			setupTime(0), kernelTime(0), devices(NULL), byteRWSupport(true), iterations(0) {
		programName.clear();
		programName.append(clFileName);
		cl_int err = CL_SUCCESS;
		loadBinaryFile = false;
		std::vector<cl::Platform> platformList;
		cl::Platform::get(&platformList);
		std::cerr << "Platform number is: " << platformList.size() << std::endl;
		std::string platformVendor;
		platformList[0].getInfo((cl_platform_info) CL_PLATFORM_VENDOR,
				&platformVendor);
		std::cerr << "Platform is by: " << platformVendor << "\n";

		// Query platforms
		std::vector<cl::Platform> platforms;
		err == cl::Platform::get(&platforms);
		if (platforms.size() == 0) {
			std::cout << "Platform size 0\n";
			throw runtime_error("No GPU platforms found");
		}

		std::vector<cl::Platform>::iterator i;
		if (platforms.size() > 0) {
			for (i = platforms.begin(); i != platforms.end(); ++i) {
				if (!strcmp((*i).getInfo<CL_PLATFORM_VENDOR>().c_str(),
						"Advanced Micro Devices, Inc.")) {
					break;
				}

			}

		}

		cl_context_properties cps[3] = { CL_CONTEXT_PLATFORM,
				(cl_context_properties) (*i)(), 0 };

		if (NULL == (*i)()) {
			throw runtime_error("NULL platform found so Exiting Application.");
		}

		// Get list of devices on default platform and create context
		cl_context_properties properties[] = { CL_CONTEXT_PLATFORM,
				(cl_context_properties) (platforms[0])(), 0 };
		contextPtr = new cl::Context(CL_DEVICE_TYPE_GPU, properties); //Change this to CL_DEVICE_GPU to use GPU or CL_DEVICE_CPU to use CPU
		devices = contextPtr->getInfo<CL_CONTEXT_DEVICES>();

		int deviceCount = (int) devices.size();
		int j = 0;
		for (std::vector<cl::Device>::iterator i = devices.begin();
				i != devices.end(); ++i, ++j) {
			std::cout << "Device " << j << " : ";
			std::string deviceName = (*i).getInfo<CL_DEVICE_NAME>();
			std::cout << deviceName.c_str() << "\n";
		}
		std::cout << "\n";

		//Build kernel from source string
		// create a CL program using the kernel source
		appsdk::SDKFile kernelFile;
		std::string kernelPath = appsdk::getPath();
		std::string Options(" -I ");
		kernelPath.append("../cl_kernels/");
		buildOptions+=Options + kernelPath + " ";
		buildOptions += addOptions;
		cl_int status = 0;
		if (false) {  //TODO: enable loading binary file
			kernelPath.append(binFileName);
			if (kernelFile.readBinaryFromFile(kernelPath.c_str()) != SDK_SUCCESS) {
				std::string msg("Failed to load kernel file : ");
				msg.append(kernelPath);
				throw runtime_error(msg);

			}
			cl::Program::Binaries programBinary(1,
					std::make_pair((const void*) kernelFile.source().data(),
							kernelFile.source().size()));

			program = cl::Program(*contextPtr, devices, programBinary, NULL,
					&status);
			if (status != SDK_SUCCESS)
				throw runtime_error("Program::Program(Binary) failed.");

		} else {
			kernelPath.append(programName.c_str());
			if (!kernelFile.open(kernelPath.c_str())) {
				std::string msg("Failed to load kernel file : ");
				msg.append(kernelPath);
				throw runtime_error(msg);
			}
			cl::Program::Sources programSource(1,
					std::make_pair(kernelFile.source().data(),
							kernelFile.source().size()));

			program = cl::Program(*contextPtr, programSource, &status);
			if ( SDK_SUCCESS != status)
				throw runtime_error("Program::Program(Source) failed.");

		}
#ifdef NDEBUG
		buildOptions+=" -DNDEBUG";
#endif
		status = program.build(devices, buildOptions.c_str());

		if (status != CL_SUCCESS) {
			if (status == CL_BUILD_PROGRAM_FAILURE) {
				std::string str = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(
						devices[0]);

				std::cout << " \n\t\t\tBUILD LOG\n";
				std::cout
						<< " ************************************************\n";
				std::cout << str << std::endl;
				std::cout
						<< " ************************************************\n";
				throw runtime_error(str);
			}
		}
		std::string str = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(
				devices[0]);

		std::cout << " \n\t\t\tBUILD LOG\n";
		std::cout << " ************************************************\n";
		std::cout << str << std::endl;
		std::cout << " ************************************************\n";

		// Create command queue for first device
		// Set command queue properties
		cl_command_queue_properties prop = 0;
		prop |= CL_QUEUE_PROFILING_ENABLE;
		queuePtr = new  cl::CommandQueue(*contextPtr, devices[0], prop, &err);
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to create queue");

		cl_int eventStatus = CL_QUEUED;

	}

	void enqWriteBuf(std::vector<double> inVec, cl::Buffer& inBuf) {
		cl_int err = CL_SUCCESS;
		cl_mem_flags inMemFlags = CL_MEM_READ_ONLY;
		// Set input data
		cl::Event inMapEvtCtl;
		cl::Event inUnmapEvtCtl;
		inMemFlags |= CL_MEM_USE_PERSISTENT_MEM_AMD; //For AMD platform
		size_t bytesBuf = inVec.size() * sizeof(cl_double);
		inBuf = cl::Buffer(*contextPtr, inMemFlags, bytesBuf,
		NULL, &err);
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to create buffer for writing");

		void* inMapPtrCtl = NULL;


		inMapPtrCtl = queuePtr->enqueueMapBuffer(inBuf, CL_FALSE, CL_MAP_WRITE, 0,
				bytesBuf,
				NULL, &inMapEvtCtl, &err);
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to enqueue Ctl buffer");
		err = queuePtr->flush();
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to flush queue");

		cl_int eventStatus = CL_QUEUED;
		while (eventStatus != CL_COMPLETE) {
			err = inMapEvtCtl.getInfo<cl_int>(CL_EVENT_COMMAND_EXECUTION_STATUS,
					&eventStatus);
			if (CL_SUCCESS != err)
				throw runtime_error("CL_EVENT_COMMAND_EXECUTION_STATUS failed");
		}

		memcpy(inMapPtrCtl, &inVec[0], bytesBuf);
		err = queuePtr->enqueueUnmapMemObject(inBuf, inMapPtrCtl,
		NULL, &inUnmapEvtCtl);
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to enqueueUnmapMemObject");

		err = queuePtr->flush();
		if (CL_SUCCESS != err)
			throw runtime_error("Unable to flush queue");

	}

	void checkSuccess(cl_int retCode, const char *msg) {
		if (CL_SUCCESS != retCode)
			throw std::runtime_error(msg);
	}

	/**
	 * In order to read the buffer the size of outVec has to BE PRESET to the size of the
	 * incoming vector otherwise the function has no way of knowing how big the vector should be
	 */
	void readBuf(std::vector<double>& outVec, const cl::Buffer& outputBuf) {
		void* outMapPtr = NULL;
		cl::Event outMapEvt;
		cl::Event outUnmapEvt;
		cl_int err;
		outMapPtr = queuePtr->enqueueMapBuffer(outputBuf, CL_FALSE, CL_MAP_READ, 0,
				outVec.size() * sizeof(cl_double),
				NULL, &outMapEvt, &err);
		checkSuccess(err,
				"cl::CommandQueue.enqueueMapBuffer failed. (outputBuf).");

		err = queuePtr->flush();
		checkSuccess(err, "cl::CommandQueue.flush failed.");

		cl_int eventStatus = CL_QUEUED;
		while (eventStatus != CL_COMPLETE) {
			err = outMapEvt.getInfo<cl_int>(CL_EVENT_COMMAND_EXECUTION_STATUS,
					&eventStatus);
			checkSuccess(err,
					"cl:Event.getInfo(CL_EVENT_COMMAND_EXECUTION_STATUS) failed.");
		}

		memcpy(&outVec[0], outMapPtr, sizeof(cl_double) *outVec.size());

		err = queuePtr->enqueueUnmapMemObject(outputBuf, outMapPtr,
		NULL, &outUnmapEvt);
		checkSuccess(err, "cl::CommandQueue.enqueueUnmapMemObject (outputBuf)");

		err = queuePtr->flush();
		checkSuccess(err , "cl::CommandQueue.flush failed.");

		eventStatus = CL_QUEUED;
		while (eventStatus != CL_COMPLETE) {
			err = outUnmapEvt.getInfo<cl_int>(CL_EVENT_COMMAND_EXECUTION_STATUS,
					&eventStatus);
			checkSuccess(err,
					"cl:Event.getInfo(CL_EVENT_COMMAND_EXECUTION_STATUS) failed.");
		}

	}

	void runJob(const cl::Kernel& kernel, size_t locWorkItems, size_t groupSize) {
		cl::Event ndrEvt;
		cl_int numLocalWorkItems = locWorkItems;
		// Number of work items in each local work group
		cl::NDRange localSize(numLocalWorkItems);
		// Number of total work items - localSize must be divisor
		cl::NDRange globalSize(numLocalWorkItems * groupSize);

		// Enqueue kernel
		cl_int err = queuePtr->enqueueNDRangeKernel(kernel, cl::NullRange,
				globalSize, localSize,
				NULL, &ndrEvt);
		checkSuccess(err, "Unable to enqueue Kernel");
		// Block until kernel completion
		cl_int eventStatus = CL_QUEUED;
		while (eventStatus != CL_COMPLETE) {
			err = ndrEvt.getInfo<cl_int>(CL_EVENT_COMMAND_EXECUTION_STATUS,
					&eventStatus);
			checkSuccess(err,
					"cl:Event.getInfo(CL_EVENT_COMMAND_EXECUTION_STATUS) failed.");
		}
		kernelTime = 0;
		// Calculate performance
		cl_ulong startTime;
		cl_ulong endTime;
		err = ndrEvt.getProfilingInfo<cl_ulong>(CL_PROFILING_COMMAND_START,
				&startTime);
		checkSuccess(err, "cl::Event.getProfilingInfo failed.(startTime).");

		err = ndrEvt.getProfilingInfo<cl_ulong>(CL_PROFILING_COMMAND_END,
				&endTime);
		checkSuccess(err, "cl::Event.getProfilingInfo failed.(endTime).");

		// Get performance numbers
		double sec = 1e-9 * (endTime - startTime);
		kernelTime += sec;

	}

	virtual ~DynOptGPU() {
		delete contextPtr;
		delete queuePtr;
	}

};

}; //namespace
#endif
